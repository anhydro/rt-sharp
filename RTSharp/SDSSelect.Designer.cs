﻿namespace RTSharp
{
	partial class SDSSelect
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SDSSelect));
			this.cmb_sds = new System.Windows.Forms.ComboBox();
			this.b_ok = new System.Windows.Forms.Button();
			this.b_cancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cmb_sds
			// 
			this.cmb_sds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cmb_sds.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmb_sds.FormattingEnabled = true;
			this.cmb_sds.Location = new System.Drawing.Point(12, 12);
			this.cmb_sds.Name = "cmb_sds";
			this.cmb_sds.Size = new System.Drawing.Size(207, 21);
			this.cmb_sds.TabIndex = 0;
			// 
			// b_ok
			// 
			this.b_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_ok.Location = new System.Drawing.Point(225, 10);
			this.b_ok.Name = "b_ok";
			this.b_ok.Size = new System.Drawing.Size(75, 23);
			this.b_ok.TabIndex = 1;
			this.b_ok.Text = "OK";
			this.b_ok.UseVisualStyleBackColor = true;
			this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
			// 
			// b_cancel
			// 
			this.b_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_cancel.Location = new System.Drawing.Point(306, 10);
			this.b_cancel.Name = "b_cancel";
			this.b_cancel.Size = new System.Drawing.Size(75, 23);
			this.b_cancel.TabIndex = 2;
			this.b_cancel.Text = "Cancel";
			this.b_cancel.UseVisualStyleBackColor = true;
			this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
			// 
			// SDSSelect
			// 
			this.AcceptButton = this.b_ok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(393, 52);
			this.Controls.Add(this.b_cancel);
			this.Controls.Add(this.b_ok);
			this.Controls.Add(this.cmb_sds);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(2147483647 / 2 /* ?????????????? */, 91);
			this.MinimumSize = new System.Drawing.Size(16, 91);
			this.Name = "SDSSelect";
			this.Text = "Select server";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox cmb_sds;
		private System.Windows.Forms.Button b_ok;
		private System.Windows.Forms.Button b_cancel;
	}
}