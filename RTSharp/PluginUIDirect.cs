﻿using BrightIdeasSoftware;
using RTSharpIFace;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace RTSharp
{
	[RTSPluginBridge]
	class PluginUIDirect : IGUIDirect
	{
		public Action<float, OLVListSubItem> RatioColoring {
			get {
				return Global.RatioColoringEx;
			}
			set {
				Global.RatioColoringEx = value;
			}
		}

		public dynamic GetCoreObject(string el)
		{
			var field = Program.MainForm.GetType().GetField(el, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			if (field == null)
				return null;
			return field.GetValue(Program.MainForm);
		}

		public Control GetCoreControl(string el)
		{
			return GetCoreObject(el) as Control;
		}

		public ToolStripItem GetCoreToolStripItemControl(string el)
		{
			return GetCoreObject(el) as ToolStripItem;
		}

		public Form GetCore() => Program.MainForm;
	}
}
