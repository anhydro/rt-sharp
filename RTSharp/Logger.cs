﻿using log4net;
using System;
using System.Runtime.CompilerServices;
using RTSharpIFace;

namespace RTSharp
{
	[RTSPluginBridge]
	class Logger
	{
		public static void Log(LOG_LEVEL Level, string Message, [CallerMemberName]string Source = "Unknown")
		{
			var log = LogManager.GetLogger(Source);
			
			switch (Level) {
				case LOG_LEVEL.DEBUG:
					log.Debug(Message);
					break;
				case LOG_LEVEL.INFO:
					log.Info(Message);
					break;
				case LOG_LEVEL.WARN:
					log.Warn(Message);
					break;
				case LOG_LEVEL.ERROR:
					log.Error(Message);
					break;
				case LOG_LEVEL.FATAL:
					log.Fatal(Message);
					break;
			}
		}

		public static void LogException(LOG_LEVEL Level, Exception Ex, [CallerMemberName]string Source = "Unknown")
		{
			var log = LogManager.GetLogger(Source);
			switch (Level) {
				case LOG_LEVEL.DEBUG:
					log.Debug(Ex);
					break;
				case LOG_LEVEL.INFO:
					log.Info(Ex);
					break;
				case LOG_LEVEL.WARN:
					log.Warn(Ex);
					break;
				case LOG_LEVEL.ERROR:
					log.Error(Ex);
					break;
				case LOG_LEVEL.FATAL:
					log.Fatal(Ex);
					break;
			}
		}
	}
}
