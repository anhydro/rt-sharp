﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BrightIdeasSoftware;
using System.Windows.Forms;
using System.Collections;
using RTSharpIFace;

namespace RTSharp
{
	class TorrentCustomSortingDataSource : FastObjectListDataSource
	{
		public TorrentCustomSortingDataSource(FastObjectListView listView) : base(listView) { }

		private OLVColumn _SortColumn;
		public OLVColumn SortColumn {
			get {
				return _SortColumn;
			}
			set {
				_SortColumn = value;
			}
		}

		public override void AddObjects(ICollection modelObjects)
		{
			base.AddObjects(modelObjects);
		}

		public override void ApplyFilters(IModelFilter iModelFilter, IListFilter iListFilter)
		{
			base.ApplyFilters(iModelFilter, iListFilter);
		}

		public override object GetNthObject(int n)
		{
			return base.GetNthObject(n);
		}

		public override int GetObjectCount()
		{
			return base.GetObjectCount();
		}

		public override int GetObjectIndex(object model)
		{
			return base.GetObjectIndex(model);
		}

		public override void InsertObjects(int index, ICollection modelObjects)
		{
			base.InsertObjects(index, modelObjects);
		}

		public override void RemoveObjects(ICollection modelObjects)
		{
			base.RemoveObjects(modelObjects);
		}

		public override int SearchText(string text, int first, int last, OLVColumn column)
		{
			return base.SearchText(text, first, last, column);
		}

		public override void SetObjects(IEnumerable collection)
		{
			base.SetObjects(collection);
		}

		public override void UpdateObject(int index, object modelObject)
		{
			base.UpdateObject(index, modelObject);
		}

		public override void Sort(OLVColumn column, SortOrder sortOrder)
		{
			base.Sort(column, sortOrder);
			/*List<TORRENT> objects = listView.Objects.Cast<TORRENT>().ToList();

			var cmp = new Comparison<TORRENT>((x, y) => x.Owner.UniqueGUID.CompareTo(y.Owner.UniqueGUID));
			var cmp2 = new HashComparer();
			var cmp3 = new Comparison<TORRENT>((x, y) => cmp2.Compare(x.Hash, y.Hash));
			var cmp4 = new Comparison<TORRENT>((x, y) => x.AddedOnDate.CompareTo(y.AddedOnDate));

			objects.Sort(cmp);
			
			//objects.Sort(cmp3);

			if (sortOrder != SortOrder.None)
				objects.Sort(new ModelObjectComparer(column, sortOrder, null, SortOrder.Ascending));

			RebuildIndexMap();*/
		}
	}
}
