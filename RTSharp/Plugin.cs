﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MadMilkman.Ini;
using MaxMind.GeoIP2;
using RTSharp;
using RTSharpIFace;
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Drawing;

internal class PluginAssembly
{
	public Assembly Asm;
	public IPlugin IFace;

	public PluginAssembly(Assembly Asm, IPlugin IFace)
	{
		this.Asm = Asm;
		this.IFace = IFace;
	}
}

internal class SystemDataSupplierAssembly
{
	public Assembly Asm;
	public ISDS SDS;

	public SystemDataSupplierAssembly(Assembly Asm, ISDS SDS)
	{
		this.Asm = Asm;
		this.SDS = SDS;
	}

	public override string ToString()
	{
		return SDS.FriendlyName + " (" + SDS.UniqueGUID + ")";
	}
}

public class PGUI : IGUI
{
	readonly PluginUIDirect _direct;
	public PGUI()
	{
		_direct = new PluginUIDirect();
	}
	public IGUIDirect Direct => _direct;

	public Func<float, Tuple<Color?, Color?>> RatioColoring {
		get {
			return Global.RatioColoring;
		}
		set {
			Global.RatioColoring = value;
		}
	}

	public void AddToolbarButton(ToolStripItem Item)
	{
		Program.MainForm.menu_main.Items.Add(Item);
	}


}

public class PRTSharp : IRTSharp {
	string IRTSharp.Version => Application.ProductVersion;
	Form IRTSharp.MainForm => Program.MainForm;

	void IRTSharp.ShowTrayMessage(string Title, string Message, ToolTipIcon Icon) => Program.MainForm.ShowTrayMessage(Title, Message, Icon);

	public async Task SetStatus(string In, bool Error) => await Program.MainForm.SetStatus(In, Error);

	List<ISDS> IRTSharp.SDSS => Global.SDSS.Select(x => x.SDS).ToList();
	List<IPlugin> IRTSharp.Plugins => Global.Plugins.Select(x => x.IFace).ToList();
	DatabaseReader IRTSharp.CountryDB => IniSettings.CountryDB;
	Dictionary<Tuple<Guid, string>, Func<dynamic, string>> IRTSharp.FormatFxs => Global.FormatFxs;
	public List<TORRENT> Torrents => Program.MainForm.Torrents;

	IniFile IRTSharp.GetPluginSettings()
	{
		var cfg = new IniFile();
		try {
			cfg.Load(System.IO.Path.Combine("plugins", "configs", plugin.UniqueGUID + ".cfg"));
		} catch {
			// ignored
		}

		return cfg;
	}
	void IRTSharp.SavePluginSettings(IniFile cfg)
	{
		cfg.Save(System.IO.Path.Combine("plugins", "configs", plugin.UniqueGUID + ".cfg"));
	}

	public void Log(LOG_LEVEL Level, string Message, [CallerMemberName] string Source = "Unknown")
	{
		Logger.Log(Level, Message, "[" + plugin.Name + " (" + plugin.UniqueGUID + ")] " + Source);
	}

	public void LogException(LOG_LEVEL Level, Exception Ex, [CallerMemberName] string Source = "Unknown")
	{
		Logger.LogException(Level, Ex, "[" + plugin.Name + " (" + plugin.UniqueGUID + ")] " + Source);
	}

	public IGUI Gui { get; }

	readonly IPlugin plugin;

	public PRTSharp(IPlugin Plugin)
	{
		plugin = Plugin;
		Gui = new PGUI();
	}
}