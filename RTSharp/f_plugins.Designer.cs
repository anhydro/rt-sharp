﻿namespace RTSharp {
	partial class f_plugins {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_plugins));
			this.lv_plugins = new System.Windows.Forms.ListView();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.t_guid = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.b_settings = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.t_version = new System.Windows.Forms.TextBox();
			this.t_author = new System.Windows.Forms.TextBox();
			this.t_desc = new System.Windows.Forms.TextBox();
			this.t_uniqGuid = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.t_name = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// lv_plugins
			// 
			this.lv_plugins.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_plugins.FullRowSelect = true;
			this.lv_plugins.Location = new System.Drawing.Point(0, 0);
			this.lv_plugins.MultiSelect = false;
			this.lv_plugins.Name = "lv_plugins";
			this.lv_plugins.Size = new System.Drawing.Size(179, 193);
			this.lv_plugins.TabIndex = 0;
			this.lv_plugins.UseCompatibleStateImageBehavior = false;
			this.lv_plugins.View = System.Windows.Forms.View.Details;
			this.lv_plugins.SelectedIndexChanged += new System.EventHandler(this.lv_plugins_SelectedIndexChanged);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer1.Location = new System.Drawing.Point(12, 12);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.lv_plugins);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.t_guid);
			this.splitContainer1.Panel2.Controls.Add(this.label7);
			this.splitContainer1.Panel2.Controls.Add(this.b_settings);
			this.splitContainer1.Panel2.Controls.Add(this.label6);
			this.splitContainer1.Panel2.Controls.Add(this.label5);
			this.splitContainer1.Panel2.Controls.Add(this.label4);
			this.splitContainer1.Panel2.Controls.Add(this.t_version);
			this.splitContainer1.Panel2.Controls.Add(this.t_author);
			this.splitContainer1.Panel2.Controls.Add(this.t_desc);
			this.splitContainer1.Panel2.Controls.Add(this.t_uniqGuid);
			this.splitContainer1.Panel2.Controls.Add(this.label3);
			this.splitContainer1.Panel2.Controls.Add(this.t_name);
			this.splitContainer1.Panel2.Controls.Add(this.label2);
			this.splitContainer1.Size = new System.Drawing.Size(537, 193);
			this.splitContainer1.SplitterDistance = 179;
			this.splitContainer1.TabIndex = 1;
			// 
			// t_guid
			// 
			this.t_guid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_guid.Location = new System.Drawing.Point(91, 133);
			this.t_guid.Name = "t_guid";
			this.t_guid.ReadOnly = true;
			this.t_guid.Size = new System.Drawing.Size(260, 20);
			this.t_guid.TabIndex = 15;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(9, 136);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(40, 13);
			this.label7.TabIndex = 14;
			this.label7.Text = "GUID: ";
			// 
			// b_settings
			// 
			this.b_settings.Location = new System.Drawing.Point(3, 167);
			this.b_settings.Name = "b_settings";
			this.b_settings.Size = new System.Drawing.Size(348, 23);
			this.b_settings.TabIndex = 13;
			this.b_settings.Text = "Settings";
			this.b_settings.UseVisualStyleBackColor = true;
			this.b_settings.Click += new System.EventHandler(this.b_settings_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(9, 110);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(48, 13);
			this.label6.TabIndex = 12;
			this.label6.Text = "Version: ";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(9, 84);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(44, 13);
			this.label5.TabIndex = 11;
			this.label5.Text = "Author: ";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(9, 58);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(66, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Description: ";
			// 
			// t_version
			// 
			this.t_version.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_version.Location = new System.Drawing.Point(91, 107);
			this.t_version.Name = "t_version";
			this.t_version.ReadOnly = true;
			this.t_version.Size = new System.Drawing.Size(260, 20);
			this.t_version.TabIndex = 9;
			// 
			// t_author
			// 
			this.t_author.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_author.Location = new System.Drawing.Point(91, 81);
			this.t_author.Name = "t_author";
			this.t_author.ReadOnly = true;
			this.t_author.Size = new System.Drawing.Size(260, 20);
			this.t_author.TabIndex = 8;
			// 
			// t_desc
			// 
			this.t_desc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_desc.Location = new System.Drawing.Point(91, 55);
			this.t_desc.Name = "t_desc";
			this.t_desc.ReadOnly = true;
			this.t_desc.Size = new System.Drawing.Size(260, 20);
			this.t_desc.TabIndex = 7;
			// 
			// t_uniqGuid
			// 
			this.t_uniqGuid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_uniqGuid.Location = new System.Drawing.Point(91, 3);
			this.t_uniqGuid.Name = "t_uniqGuid";
			this.t_uniqGuid.ReadOnly = true;
			this.t_uniqGuid.Size = new System.Drawing.Size(260, 20);
			this.t_uniqGuid.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(9, 6);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(77, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Unique GUID: ";
			// 
			// t_name
			// 
			this.t_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_name.Location = new System.Drawing.Point(91, 29);
			this.t_name.Name = "t_name";
			this.t_name.ReadOnly = true;
			this.t_name.Size = new System.Drawing.Size(260, 20);
			this.t_name.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(9, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(76, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Display name: ";
			// 
			// f_plugins
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(561, 217);
			this.Controls.Add(this.splitContainer1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "f_plugins";
			this.Text = "Connections";
			this.Load += new System.EventHandler(this.f_plugins_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView lv_plugins;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TextBox t_uniqGuid;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox t_name;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button b_settings;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox t_version;
		private System.Windows.Forms.TextBox t_author;
		private System.Windows.Forms.TextBox t_desc;
		private System.Windows.Forms.TextBox t_guid;
		private System.Windows.Forms.Label label7;
	}
}