﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace RTSharp
{
	public partial class f_about : Form
	{
		public f_about()
		{
			InitializeComponent();
		}

		readonly int[] side = new int[3];

		private void t_color_Tick(object sender, EventArgs e)
		{
			var c = new int[3];

			c[0] = l_proj.ForeColor.R + side[0];
			c[1] = l_proj.ForeColor.G + side[1];
			c[2] = l_proj.ForeColor.B + side[2];

			for (int x = 0;x < 3;x++) {
				if (c[x] > 255) {
					c[x] = 255;
					do {
						side[x] = Utils.random.Next(-2, 2);
					} while (side[x] == 0);
				} else if (c[x] < 0) {
					c[x] = 0;
					do {
						side[x] = Utils.random.Next(-2, 2);
					} while (side[x] == 0);
				}
			}

			l_proj.ForeColor = Color.FromArgb(c[0], c[1], c[2]);
		}

		private void f_about_Load(object sender, EventArgs e)
		{
			for (int x = 0;x < 3;x++) {
				do {
					side[x] = Utils.random.Next(-2, 2);
				} while (side[x] == 0);
			}
		}
	}
}
