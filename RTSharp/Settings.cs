﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using MadMilkman.Ini;
using System.Windows.Forms;

using MaxMind.GeoIP2;

using static RTSharpIFace.Utils;
using RTSharpIFace;

namespace RTSharp {

	public static class IniSettings {

		public static DatabaseReader CountryDB;

		public static void LoadSettings() {
			var exAt = "";
			try {
				exAt = "Images";
				Program.MainForm.ImgList.Images.Add("ICON_ALL", Image.FromFile(GetFileWildcard("images\\all.*")));
				Program.MainForm.ImgList.Images.Add("ICON_DOWNLOAD", Image.FromFile(GetFileWildcard("images\\download.*")));
				Program.MainForm.ImgList.Images.Add("ICON_UPLOAD", Image.FromFile(GetFileWildcard("images\\upload.*")));
				Program.MainForm.ImgList.Images.Add("ICON_STOP", Image.FromFile(GetFileWildcard("images\\stop.*")));
				Program.MainForm.ImgList.Images.Add("ICON_ACTIVE", Image.FromFile(GetFileWildcard("images\\active.*")));
				Program.MainForm.ImgList.Images.Add("ICON_INACTIVE", Image.FromFile(GetFileWildcard("images\\inactive.*")));
				Program.MainForm.ImgList.Images.Add("ICON_LABEL", Image.FromFile(GetFileWildcard("images\\label.*")));
				Program.MainForm.ImgList.Images.Add("ICON_TRACKER", Image.FromFile(GetFileWildcard("images\\tracker.*")));
				Program.MainForm.ImgList.Images.Add("ICON_FD", Image.FromFile(GetFileWildcard("images\\fd.*")));
				Program.MainForm.ImgList.Images.Add("ICON_PRIVATE", Image.FromFile(GetFileWildcard("images\\private.*")));

				var flags = GetFilesWildcard("images\\flags\\*.png");
				foreach (var t in flags)
					Program.MainForm.CompanyImgs.Images.Add(Path.GetFileNameWithoutExtension(t), Image.FromFile(t));

				exAt = "MMDB";
				CountryDB = new DatabaseReader("mmdb\\GeoLite2-Country.mmdb");

				if (!System.IO.File.Exists("config.cfg")) {
					SaveSettings();
					return;
				}

				var cfg = new IniFile();
				cfg.Load("config.cfg");

				exAt = "Section: Interface";
				var iface = cfg.Sections["Interface"];

				exAt = "Interface: MainDisplayData";
				if (iface.Keys["MainDisplayData"] != null) {
					var mainDisplayData = iface.Keys["MainDisplayData"].Value;
					if (!Program.MainForm.lv_torrents.RestoreState(Convert.FromBase64String(mainDisplayData)))
						throw new Exception("Failed to parse MainDisplayData");
				}

				exAt = "Interface: MainDisplaySecondarySorting";
				if (iface.Keys["MainDisplaySecondarySorting"] != null) {
					int index;
					if (!iface.Keys["MainDisplaySecondarySorting"].TryParseValue(out index))
						throw new Exception("Failed to parse MainDisplaySecondarySorting (int)");

					Program.MainForm.lv_torrents.SecondarySortColumn = Program.MainForm.lv_torrents.GetColumn(index);
				}

				exAt = "Interface: MainDisplaySecondarySortOrder";
				if (iface.Keys["MainDisplaySecondarySortOrder"] != null) {
					SortOrder val;
					if (!iface.Keys["MainDisplaySecondarySortOrder"].TryParseValue(out val))
						throw new Exception("Failed to parse MainDisplaySecondarySortOrder (SortOrder)");

					Program.MainForm.lv_torrents.SecondarySortOrder = val;
				}

				exAt = "Interface: PeersDisplayData";
				if (iface.Keys["PeersDisplayData"] != null) {
					var peersDisplayData = iface.Keys["PeersDisplayData"].Value;
					if (!Program.MainForm.lv_peers.RestoreState(Convert.FromBase64String(peersDisplayData)))
						throw new Exception("Failed to parse PeersDisplayData");
				}

				exAt = "Interface: PeersSecondarySorting";
				if (iface.Keys["PeersSecondarySorting"] != null) {
					int index;
					if (!iface.Keys["PeersSecondarySorting"].TryParseValue(out index))
						throw new Exception("Failed to parse PeersSecondarySorting (int)");

					Program.MainForm.lv_peers.SecondarySortColumn = Program.MainForm.lv_peers.GetColumn(index);
				}

				exAt = "Interface: PeersSecondarySortOrder";
				if (iface.Keys["PeersSecondarySortOrder"] != null) {
					SortOrder val;
					if (!iface.Keys["PeersSecondarySortOrder"].TryParseValue(out val))
						throw new Exception("Failed to parse PeersSecondarySortOrder (SortOrder)");

					Program.MainForm.lv_peers.SecondarySortOrder = val;
				}

				exAt = "Interface: TrackersDisplayData";
				if (iface.Keys["TrackersDisplayData"] != null) {
					var trackersDisplayData = iface.Keys["TrackersDisplayData"].Value;
					if (!Program.MainForm.lv_trackers.RestoreState(Convert.FromBase64String(trackersDisplayData)))
						throw new Exception("Failed to parse TrackersDisplayData");
				}

				exAt = "Interface: FilesDisplayData";
				if (iface.Keys["FilesDisplayData"] != null) {
					var filesDisplayData = iface.Keys["FilesDisplayData"].Value;
					if (!Program.MainForm.dtlv_files.RestoreState(Convert.FromBase64String(filesDisplayData)))
						throw new Exception("Failed to parse FilesDisplayData");
				}

				exAt = "Interface: WindowSize";
				if (iface.Keys["WindowSize"] != null) {
					string val;
					if (!iface.Keys["WindowSize"].TryParseValue(out val))
						throw new Exception("Failed to parse WindowSize (string)");

					Program.MainForm.Size = new System.Drawing.Size(Int32.Parse(val.Split(',')[0]), Int32.Parse(val.Split(',')[1]));
				}

				exAt = "Interface: WindowState";
				if (iface.Keys["WindowState"] != null) {
					FormWindowState val;
					if (!iface.Keys["WindowState"].TryParseValue(out val))
						throw new Exception("Failed to parse WindowState (FormWindowState)");

					Program.MainForm.WindowState = val;
				}

				exAt = "Interface: WindowLoc";
				if (iface.Keys["WindowLoc"] != null) {
					string val;
					if (!iface.Keys["WindowLoc"].TryParseValue(out val))
						throw new Exception("Failed to parse WindowLoc (string)");

					Program.MainForm.Location = new Point(Int32.Parse(val.Split(',')[0]), Int32.Parse(val.Split(',')[1]));
				}

				exAt = "Interface: SplitterDistance";
				if (iface.Keys["SplitterDistance"] != null) {
					int val;
					if (!iface.Keys["SplitterDistance"].TryParseValue(out val))
						throw new Exception("Failed to parse RefreshInterval (int)");

					Program.MainForm.split_torrents.SplitterDistance = val;
				}

				exAt = "Interface: RefreshInterval";
				if (iface.Keys["RefreshInterval"] != null && !iface.Keys["RefreshInterval"].TryParseValue(out Settings.RefreshInterval))
					throw new Exception("Failed to parse RefreshInterval (int)");

				exAt = "Interface: DateFormat";
				Settings.DateFormat = iface.Keys["DateFormat"]?.Value;

				exAt = "Interface: FolderParseLimit";
				if (iface.Keys["FolderParseLimit"] != null && !iface.Keys["FolderParseLimit"].TryParseValue(out Settings.FolderParseLimit)) {
					throw new Exception("Failed to parse FolderParseLimit (int)");
				}

				exAt = "Interface: TrackerMatches";
				Settings.TrackerMatches = new Dictionary<string, string>();
				var trackersFormat = iface.Keys["TrackerMatches"]?.Value.Split(',');
				if (trackersFormat?.Length >= 2) {
					for (var x = 0; x < trackersFormat.Length; x += 2)
						Settings.TrackerMatches.Add(trackersFormat[x], trackersFormat[x + 1]);
				}

				exAt = "Interface: SDS highlighting";
				Settings.SDSHighlighting = new Dictionary<Guid, Color>();
				var highlighting = iface.Keys["SDSHighlighting"]?.Value.Split(',');
				if (highlighting?.Length >= 2) {
					for (var x = 0; x < highlighting.Length; x += 2)
						Settings.SDSHighlighting.Add(new Guid(highlighting[x]), ColorTranslator.FromHtml(highlighting[x + 1]));
				}

				exAt = "Interface: GraphPointsLimit";
				if (iface.Keys["GraphPointsLimit"] != null && !iface.Keys["GraphPointsLimit"].TryParseValue(out Settings.GraphPointsLimit))
					throw new Exception("Failed to parse GraphPointsLimit (int)");

				exAt = "Section: System";
				var sys = cfg.Sections["System"];

				exAt = "System: DomainReplacements";
				Settings.DomainReplacements = new Dictionary<string, string>();
				var domainsFormat = sys.Keys["DomainReplacements"].Value.Split(',');
				for (var x = 0; x < domainsFormat.Length; x += 2)
					Settings.DomainReplacements.Add(domainsFormat[x], domainsFormat[x + 1]);

				exAt = "System: SaveTorrentsInZip";
				if (sys.Keys["SaveTorrentsInZip"] != null && !sys.Keys["SaveTorrentsInZip"].TryParseValue(out Settings.SaveTorrentsInZip))
					throw new Exception("Failed to parse SaveTorrentsInZip (bool)");

			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.FATAL, "Corrupt config on " + exAt);
				Logger.LogException(LOG_LEVEL.FATAL, ex);

				var res = MessageBox.Show("Corrupt config: " + ex + " (on " + exAt + ")\nRestore default?", "RT#", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
				if (res == DialogResult.Yes)
					SaveSettings();
				else
					Environment.Exit(1);
			}
		}

		public static void SaveSettings(IniFile cfg = null) {
			if (cfg == null) {
				if (!System.IO.File.Exists("config.cfg"))
					System.IO.File.Create("config.cfg").Close();

				cfg = new IniFile();
				cfg.Load("config.cfg");
			} else {
				cfg.Save("config.cfg");
				return;
			}

			cfg.Sections.Remove("Interface");
			var iface = cfg.Sections.Add("Interface");

			iface.Keys.Add("MainDisplayData", Convert.ToBase64String(Program.MainForm.lv_torrents.SaveState()));
			iface.Keys.Add("MainDisplaySecondarySorting", (Program.MainForm.lv_torrents.SecondarySortColumn ?? Program.MainForm.lvc_name).Index.ToString());
			iface.Keys.Add("MainDisplaySecondarySortOrder", Program.MainForm.lv_torrents.SecondarySortOrder.ToString());
			iface.Keys.Add("PeersDisplayData", Convert.ToBase64String(Program.MainForm.lv_peers.SaveState()));
			iface.Keys.Add("TrackersDisplayData", Convert.ToBase64String(Program.MainForm.lv_trackers.SaveState()));
			iface.Keys.Add("FilesDisplayData", Convert.ToBase64String(Program.MainForm.dtlv_files.SaveState()));
			iface.Keys.Add("PeersSecondarySorting", (Program.MainForm.lv_peers.SecondarySortColumn ?? Program.MainForm.lv_peers_ip).Index.ToString());
			iface.Keys.Add("PeersSecondarySortOrder", Program.MainForm.lv_peers.SecondarySortOrder.ToString());

			iface.Keys.Add("WindowSize", Program.MainForm.Size.Width + "," + Program.MainForm.Size.Height);
			iface.Keys.Add("WindowState", Program.MainForm.WindowState.ToString());
			iface.Keys.Add("WindowLoc", Program.MainForm.Location.X + "," + Program.MainForm.Location.Y);
			iface.Keys.Add("SplitterDistance", Program.MainForm.split_torrents.SplitterDistance.ToString());

			if (Settings.RefreshInterval == -1)
				Settings.RefreshInterval = 5000;
			iface.Keys.Add("RefreshInterval", Settings.RefreshInterval.ToString());

			if (String.IsNullOrEmpty(Settings.DateFormat))
				Settings.DateFormat = "u";
			iface.Keys.Add("DateFormat", Settings.DateFormat);

			if (Settings.FolderParseLimit == -1)
				Settings.FolderParseLimit = 1000;
			iface.Keys.Add("FolderParseLimit", Settings.FolderParseLimit.ToString());

			if (Settings.TrackerMatches != null && Settings.TrackerMatches.Count > 0) {
				var trackersMatches = Settings.TrackerMatches.Select(x => x.Key + "," + x.Value).Aggregate((a, b) => a + "," + b);
				iface.Keys.Add("TrackerMatches", trackersMatches);
			}

			if (Settings.SDSHighlighting != null && Settings.SDSHighlighting.Count > 0) {
				var highlighting = Settings.SDSHighlighting.Select(x => x.Key + "," + ColorTranslator.ToHtml(x.Value)).Aggregate((a, b) => a + "," + b);
				iface.Keys.Add("SDSHighlighting", highlighting);
			}

			if (Settings.GraphPointsLimit == -1)
				Settings.GraphPointsLimit = 500;
			iface.Keys.Add("GraphPointsLimit", Settings.GraphPointsLimit.ToString());

			cfg.Sections.Remove("System");
			var sys = cfg.Sections.Add("System");

			if (Settings.DomainReplacements == null)
				Settings.DomainReplacements = new Dictionary<string, string> {{"ovh.net", "ovh.com"}};

			var domainReplacements = Settings.DomainReplacements.Select(x => x.Key + "," + x.Value).Aggregate((a, b) => a + "," + b);
			sys.Keys.Add("DomainReplacements", domainReplacements);

			sys.Keys.Add("SaveTorrentsInZip", Settings.SaveTorrentsInZip.ToString());

			cfg.Save("config.cfg");
		}
	}
}
