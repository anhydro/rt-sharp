﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharp
{
	public partial class f_settings : Form
	{
		public f_settings()
		{
			InitializeComponent();
		}

		[DllImport("user32.dll")]
		public static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

		private void f_settings_Load(object sender, EventArgs e)
		{
			SendMessage(b_assoc.Handle, 0x160C, 0, 1);
		}

		private void b_assoc_Click(object sender, EventArgs e)
		{
			ProcessStartInfo proc = new ProcessStartInfo();
			proc.FileName = Application.ExecutablePath;
			proc.Verb = "runas";
			proc.Arguments = "--assocExt";
			Process.Start(proc).WaitForExit();
		}
	}
}
