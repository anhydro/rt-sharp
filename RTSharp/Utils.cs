﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharp
{
	class Utils
	{
		public  static readonly Random random = new Random();

		public static string RandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
		}

		private static Task WaitForExitAsync(Process process,
		CancellationToken cancellationToken = default(CancellationToken))
		{
			var tcs = new TaskCompletionSource<object>();
			process.EnableRaisingEvents = true;
			process.Exited += (sender, args) => tcs.TrySetResult(null);
			if (cancellationToken != default(CancellationToken))
				cancellationToken.Register(tcs.SetCanceled);

			return tcs.Task;
		}

		public static async Task<IEnumerable<string>> LaunchProcess(string FileName, string Args, DataReceivedEventHandler callback = null)
		{
			var proc = new Process();
			proc.StartInfo.FileName = FileName;
			proc.StartInfo.Arguments = Args;
			proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			proc.StartInfo.CreateNoWindow = true;
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.RedirectStandardOutput = true;
			proc.StartInfo.RedirectStandardError = true;
			proc.Start();
			proc.BeginErrorReadLine();
			proc.BeginOutputReadLine();
			var output = new List<string>();
			proc.OutputDataReceived += (sender, e) => {
				output.Add(e.Data);
				callback?.Invoke(sender, e);
			};
			proc.ErrorDataReceived += (sender, e) => {
				output.Add(e.Data);
				callback?.Invoke(sender, e);
			};

			await WaitForExitAsync(proc);

			if (proc.ExitCode != 0)
				throw new Exception(output.Aggregate((a, b) => a + Environment.NewLine + b));

			return output;
		}

		public static void InvokeOpt(Action Fx)
		{
			if (!Program.MainForm.IsHandleCreated)
				return;

			if (Program.MainForm.InvokeRequired)
				Program.MainForm.Invoke(Fx);
			else
				Fx();
		}

		public static async Task InvokeOpt(Func<Task> Fx)
		{
			if (!Program.MainForm.IsHandleCreated)
				return;

			if (Program.MainForm.InvokeRequired)
				await (Task)Program.MainForm.Invoke(Fx);
			else
				await Fx();
		}
	}
}
