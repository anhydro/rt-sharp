﻿using System.Net;
using System.Threading.Tasks;

namespace RTSharp
{
	internal class Favicons
	{
		public static async Task<byte[]> DownloadFavicon(string Domain)
		{
			var wClient = new WebClient();

			Logger.Log(RTSharpIFace.LOG_LEVEL.DEBUG, "Downloading favicon of " + Domain);

			return await wClient.DownloadDataTaskAsync("https://www.google.com/s2/favicons?domain_url=" + Domain);
		}
	}
}
