﻿using Microsoft.Win32;
using System;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace RTSharp {
	static class Program {
		public static f_main MainForm;
		public static Mutex Mtx = new Mutex(false, "RTSharp");

		[DllImport("shell32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern void SHChangeNotify(uint wEventId, uint uFlags, IntPtr dwItem1, IntPtr dwItem2);

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args) {

			Environment.CurrentDirectory = Path.GetDirectoryName(Application.ExecutablePath);

			Action pre = () => {
				log4net.Config.XmlConfigurator.Configure();
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Application.SetUnhandledExceptionMode(UnhandledExceptionMode.Automatic);
			};

			var notRunning = Mtx.WaitOne(TimeSpan.Zero, true);

			if (args.Length > 0) {
				if (args[0] == "--assocExt") {
					try {
						var t = Registry.ClassesRoot.CreateSubKey(".torrent");
						t.SetValue("", "rtsharp.torrent", RegistryValueKind.String);
						t.SetValue("Content Type", "application/x-bittorrent", RegistryValueKind.String);
						t.Close();

						var rts = Registry.ClassesRoot.CreateSubKey("rtsharp.torrent");
						rts.SetValue("", "RT# torrent file", RegistryValueKind.String);
						rts.SetValue("FriendlyTypeName", "RT# torrent file", RegistryValueKind.String);
						rts.CreateSubKey("shell\\open\\command").SetValue("", "\"" + Application.ExecutablePath + "\" \"%1\"", RegistryValueKind.String);
						rts.CreateSubKey("DefaultIcon").SetValue("", "\"" + Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "images", "ico.ico") + "\"", RegistryValueKind.String);
						rts.Close();

						var ovr = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\.torrent", true);
						ovr.DeleteSubKey("OpenWithProgids", false);
						ovr.Close();
					} catch { }

					SHChangeNotify(0x08000000, 0x0000, IntPtr.Zero, IntPtr.Zero);

					MessageBox.Show(".torrent file extension is now associated with RT#", "RT#", MessageBoxButtons.OK, MessageBoxIcon.Information);

					return;
				} else {
					if (notRunning) {
						pre();
						Application.Run((MainForm = new f_main(args[0])));
						Mtx.ReleaseMutex();
						return;
					} else {
						try {
							var pipe = new NamedPipeClientStream(
								".",
								"RTSharpAddTorrent",
								PipeDirection.Out,
								PipeOptions.WriteThrough,
								System.Security.Principal.TokenImpersonationLevel.Impersonation
							);
							pipe.Connect(10000);
							var b = System.Text.Encoding.UTF8.GetBytes("ATOR" + args[0] + "\n");
							pipe.Write(b, 0, b.Length);
							pipe.Close();
						} catch { }
						return;
					}
				}
			}

			pre();
			Application.Run((MainForm = new f_main()));
			Mtx.ReleaseMutex();
		}
	}
}
