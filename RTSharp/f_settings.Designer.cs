﻿namespace RTSharp
{
	partial class f_settings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_settings));
			this.b_assoc = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// b_assoc
			// 
			this.b_assoc.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.b_assoc.Location = new System.Drawing.Point(12, 12);
			this.b_assoc.Name = "b_assoc";
			this.b_assoc.Size = new System.Drawing.Size(185, 23);
			this.b_assoc.TabIndex = 0;
			this.b_assoc.Text = "Associate RT# with .torrent files";
			this.b_assoc.UseVisualStyleBackColor = true;
			this.b_assoc.Click += new System.EventHandler(this.b_assoc_Click);
			// 
			// f_settings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(212, 45);
			this.Controls.Add(this.b_assoc);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "f_settings";
			this.Text = "Settings";
			this.Load += new System.EventHandler(this.f_settings_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button b_assoc;
	}
}