﻿using RTSharpIFace;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Reflection;

using static RTSharp.Utils;
using static RTSharpIFace.Utils;
using static RTSharp.Global;
using BrightIdeasSoftware;
using System.Drawing;

namespace RTSharp
{
	internal class File
	{
		static readonly object FilesCacheLock = new object();

		static readonly Dictionary<FileCacheKey, List<FILES>> FilesCache = new Dictionary<FileCacheKey, List<FILES>>();

		class FileCacheKey : IEquatable<FileCacheKey>
		{
			public readonly byte[] Hash;
			public Guid UniqSDS;

			public FileCacheKey(byte[] Hash, Guid UniqSDS)
			{
				this.Hash = Hash;
				this.UniqSDS = UniqSDS;
			}

			[SuppressMessage("ReSharper", "LoopCanBeConvertedToQuery")]
			[SuppressMessage("ReSharper", "ForCanBeConvertedToForeach")]
			[SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
			public override int GetHashCode()
			{
				unchecked {
					const int p = 16777619;
					var hash = (int)2166136261;
					var b = UniqSDS.ToByteArray();

					for (int i = 0; i < Hash.Length; i++)
						hash = (hash ^ Hash[i]) * p;
					for (int i = 0; i < b.Length; i++)
						hash = (hash ^ b[i]) * p;

					hash += hash << 13;
					hash ^= hash >> 7;
					hash += hash << 3;
					hash ^= hash >> 17;
					hash += hash << 5;

					return hash;
				}
			}

			public override bool Equals(object other)
			{
				return Equals(other as FileCacheKey);
			}

			public bool Equals(FileCacheKey other)
			{
				Debug.Assert(other != null);
				return other.Hash.SequenceEqual(Hash) && other.UniqSDS.Equals(UniqSDS);
			}
		}

		[SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
		public static void FetchFilesForSelectedTorrent()
		{
			var t = Torrent.GetSelectedTorrents();
			var tEnumer = t as TORRENT[] ?? t.ToArray();

			if (tEnumer.Length == 0)
				return;

			var torrent = tEnumer.First();

			if ((torrent.State.HasFlag(TORRENT_STATE.ACTIVE) || torrent.State.HasFlag(TORRENT_STATE.HASHING)) && torrent.Done != 100.0f) {
				Task.Run(() => {
					FetchFiles(torrent).ContinueWith(x => {
						Logger.Log(LOG_LEVEL.ERROR, "Fetch files failed on " + torrent.Name);
						Logger.LogException(LOG_LEVEL.ERROR, x.Exception);
					}, TaskContinuationOptions.OnlyOnFaulted);
				});
			}
		}

		static readonly byte[] CACHE_SEPERATOR = GetBytesASCII("SEPERATOR\x00\xFF");

		public static void SaveFileCache()
		{
			lock (FilesCacheLock) {
				try {
					var bf = new BinaryFormatter();
					using (var filesCache = new BinaryWriter(System.IO.File.Open("cache/files.dat", FileMode.Truncate))) {
						List<Guid> sdssTable = new List<Guid>();
						foreach (var kv in FilesCache) {
							if (!sdssTable.Contains(kv.Key.UniqSDS)) {
								sdssTable.Add(kv.Key.UniqSDS);
							}
						}

						filesCache.Write(sdssTable.Count);
						foreach (var sdssGuid in sdssTable)
							filesCache.Write(sdssGuid.ToByteArray());

						foreach (var kv in FilesCache) {
							filesCache.Write(kv.Key.Hash);
							filesCache.Write(sdssTable.IndexOf(kv.Key.UniqSDS));
							filesCache.Write(kv.Value.Count);

							foreach (var f in kv.Value) {
								filesCache.Write(f.Parent);
								filesCache.Write(f.ArrayIndex);
								filesCache.Write(f.Children.Count);
								foreach (var t in f.Children)
									filesCache.Write(t);

								using (var mem = new MemoryStream()) {
									bf.Serialize(mem, f.Item.ID);
									var bin = mem.ToArray();
									filesCache.Write((int)mem.Length);
									filesCache.Write(bin);
								}

								filesCache.Write(f.Item.Name);
								filesCache.Write(f.Item.Size);
								filesCache.Write(f.Item.DownloadedChunks);
								filesCache.Write((byte)f.Item.Priority);
								filesCache.Write((byte)f.Item.DownloadStrategy);
								filesCache.Write(f.Item.Directory);
							}

							filesCache.Write(CACHE_SEPERATOR);
						}
					}
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Error saving files cache.");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}
		}

		sealed class AssemblyObjectBinder : SerializationBinder
		{
			readonly Assembly Target;

			public override Type BindToType(string assemblyName, string typeName)
			{
				// HACKHACK
				if (Target.FullName.Substring(0, Target.FullName.IndexOf("Version=")) == assemblyName.Substring(0, Target.FullName.IndexOf("Version=")))
					return Target.GetTypes().First(x => x.FullName == typeName);

				return null;
			}
			public AssemblyObjectBinder(Assembly In)
			{
				Target = In;
			}
		}

		public static void LoadFileCache(WaitingBox wait)
		{
			int progress = 0;
			FileStream h;
			try {
				h = System.IO.File.Open("cache/files.dat", FileMode.OpenOrCreate);
			} catch (Exception ex) {
				Logger.Log(LOG_LEVEL.FATAL, "FAILED TO OPEN OR CREATE cache/files.dat");
				Logger.LogException(LOG_LEVEL.FATAL, ex);
				return;
			}
			using (var filesCache = new BinaryReader(h)) {
				List<Guid> sdssTable;
				if (filesCache.BaseStream.Length < 4) {
					Logger.Log(LOG_LEVEL.WARN, "Files cache empty");
					return;
				}

				try {
					var sdssTableLen = filesCache.ReadInt32();
					sdssTable = new List<Guid>(sdssTableLen);
					for (int x = 0; x < sdssTableLen; x++)
						sdssTable.Add(new Guid(filesCache.ReadBytes(16)));
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to construct SDSS table");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					return;
				}

				while (filesCache.BaseStream.Position != filesCache.BaseStream.Length) {
					try {
						var hash = filesCache.ReadBytes(20);
						var owner = sdssTable[filesCache.ReadInt32()];
						var flCount = filesCache.ReadInt32();
						var fls = new List<FILES>();
						for (var i = 0; i < flCount; i++) {
							var fl = new FILES {
								Parent = filesCache.ReadInt32(),
								ArrayIndex = filesCache.ReadInt32()
							};
							var childrenCount = filesCache.ReadInt32();
							fl.Children = new List<int>();
							for (int x = 0; x < childrenCount; x++)
								fl.Children.Add(filesCache.ReadInt32());

							object ID;

							var assembly = SDSS.FirstOrDefault(x => x.SDS.UniqueGUID == owner);
						
							if (assembly == null) {
								filesCache.ReadBytes(filesCache.ReadInt32());
								filesCache.ReadString();
								filesCache.ReadBytes(19 + CACHE_SEPERATOR.Length);
								continue;
							}

							using (var mem = new MemoryStream()) {
								var bf = new BinaryFormatter {
									Binder = new AssemblyObjectBinder(assembly.Asm)
								};
								var sz = filesCache.ReadInt32();
								var bin = filesCache.ReadBytes(sz);
								mem.Write(bin, 0, sz);
								mem.Seek(0, SeekOrigin.Begin);
								ID = bf.Deserialize(mem);
							}

							var f = new FILE(null, owner, ID) {
								Name = filesCache.ReadString(),
								Size = filesCache.ReadUInt64(),
								DownloadedChunks = filesCache.ReadUInt64(),
								Priority = (FILE.PRIORITY) filesCache.ReadByte(),
								DownloadStrategy = (FILE.DOWNLOAD_STRATEGY) filesCache.ReadByte(),
								Directory = filesCache.ReadBoolean()
							};

							fl.Item = f;
							fls.Add(fl);
						}

						var sep = filesCache.ReadBytes(CACHE_SEPERATOR.Length);
						if (!sep.SequenceEqual(CACHE_SEPERATOR))
							throw new FormatException(
								"Cache seperator not found (found " + Convert.ToBase64String(sep) + ", expected " + Convert.ToBase64String(CACHE_SEPERATOR) + ")");

						// ReSharper disable once InconsistentlySynchronizedField
						FilesCache.Add(new FileCacheKey(hash, owner), fls);
					} catch (Exception ex) {
						// Align to next entry on error
						Logger.Log(LOG_LEVEL.ERROR, "Failure reading file cache (current offset " + filesCache.BaseStream.Position + "), trying to recover...");
						Logger.LogException(LOG_LEVEL.ERROR, ex);

						var sep = CACHE_SEPERATOR;
						try {
							int current = 0;
							while (filesCache.BaseStream.Position != filesCache.BaseStream.Length) {
								var b = filesCache.ReadByte();
								if (sep[current] == b) {
									current++;
								} else
									current = 0;

								if (current == sep.Length) {
									Logger.Log(LOG_LEVEL.INFO, "Recovered successfully, reading from offset " + filesCache.BaseStream.Position);
									break;
								}
							}
						} catch (Exception ex2) {
							Logger.Log(LOG_LEVEL.ERROR, "Failed to recover");
							Logger.LogException(LOG_LEVEL.ERROR, ex2);
							Logger.Log(LOG_LEVEL.ERROR, "File cache loading failed");
							return;
						}
					}
					
					progress++;

					if (progress % 100 == 0) {
						wait.UpdatePBarSingle((byte)((double)filesCache.BaseStream.Position / filesCache.BaseStream.Length * 100));
						wait.UpdateText("File cache (" + filesCache.BaseStream.Position + " / " + filesCache.BaseStream.Length + ")");
						Application.DoEvents();
					}
				}
			}
			Logger.Log(LOG_LEVEL.INFO, "File cache loaded");
		}

		public static void EvTorrentRemoved(object sender, RTSEvents.TorrentRemovedArgs e)
		{
			var key = new FileCacheKey(e.Hash, ((ISDS)sender).UniqueGUID);
			lock (FilesCacheLock) {
				if (!FilesCache.ContainsKey(key)) {
					// LOG
				}

				FilesCache.Remove(key);
			}
		}

		public static async Task FetchFiles(TORRENT torrents)
		{
			await FetchFiles(new[] { torrents }.ToList());
		}

		public static async Task FetchFiles(List<TORRENT> torrents)
		{
			lock (FilesCacheLock) {
				for (int x = 0;x < torrents.Count;x++) {
				if (torrents[x].State.HasFlag(TORRENT_STATE.SEEDING)) {
					var key = new FileCacheKey(torrents[x].Hash, torrents[x].Owner.UniqueGUID);
						if (FilesCache.ContainsKey(key)) {
							torrents[x].FileList = FilesCache[key];
							torrents[x].FileList.ForEach(f => f.Item.Parent = torrents[x]); // Uninitialized parent torrent must be set
							torrents.RemoveAt(x);
							x--;
						}
					}
				}
			}

			var sdss = Torrent.MapTorrentsToSDSS(torrents);
			var files = new Dictionary<TORRENT, List<FILES>>();
			foreach (var kv in sdss) {
				try {
					foreach (var f in (await kv.Key.GetFiles(kv.Value)))
						files.Add(f.Key, f.Value);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to get files for torrents of " + kv.Key.UniqueGUID);
					Logger.LogException(LOG_LEVEL.ERROR, ex);
				}
			}

			foreach (var kv in files) {
				if (kv.Value.Count <= Settings.FolderParseLimit && kv.Value.Count > 1) {
					kv.Value.Where(x => !x.Item.Directory).ToList().ForEach(t => {
						var curDir = t;
						do {
							kv.Value[curDir.Parent].Item.Size += t.Item.Size;
							kv.Value[curDir.Parent].Item.DownloadedChunks += t.Item.DownloadedChunks;
							kv.Value[curDir.Parent].Item.Parent = kv.Key;

							if (kv.Value[curDir.Parent].Item.DownloadStrategy == FILE.DOWNLOAD_STRATEGY.NA)
								kv.Value[curDir.Parent].Item.DownloadStrategy = t.Item.DownloadStrategy;

							kv.Value[curDir.Parent].Item.DownloadStrategy = kv.Value[curDir.Parent].Item.DownloadStrategy == t.Item.DownloadStrategy ? t.Item.DownloadStrategy : FILE.DOWNLOAD_STRATEGY.INTERNAL_NOT_SET;

							if (kv.Value[curDir.Parent].Item.Priority == FILE.PRIORITY.NA)
								kv.Value[curDir.Parent].Item.Priority = t.Item.Priority;

							kv.Value[curDir.Parent].Item.Priority = kv.Value[curDir.Parent].Item.Priority == t.Item.Priority ? t.Item.Priority : FILE.PRIORITY.INTERNAL_NOT_SET;
							curDir = kv.Value[curDir.Parent];
						} while (curDir.Parent != -1);
					});
				}

				kv.Key.FileList = kv.Value.ToList();

				if (kv.Key.State.HasFlag(TORRENT_STATE.SEEDING)) {
					var key = new FileCacheKey(kv.Key.Hash, kv.Key.Owner.UniqueGUID);
					lock (FilesCacheLock) {
						if (!FilesCache.ContainsKey(key))
							FilesCache.Add(key, kv.Key.FileList);
					}
				}

				if (kv.Key.CurrentlySelectedInUI) {
					InvokeOpt(() => {
						if (kv.Key.FileList == null)
							Program.MainForm.dtlv_files.DataSource = new List<FILES>();
						else {
							Program.MainForm.dtlv_files.DataSource = kv.Key.FileList;
							Program.MainForm.dtlv_files.Sort();
						}
					});
				}
			}
		}

		public static Dictionary<ISDS, List<FILE>> MapFilesToSDSS(IEnumerable<FILE> Files)
		{
			var list = new Dictionary<ISDS, List<FILE>>();

			foreach (var f in Files) {
				var sds = f.Parent.Owner;
				if (!list.ContainsKey(sds))
					list.Add(sds, new List<FILE>());

				list[sds].Add(f);
			}

			return list;
		}

		public static Dictionary<ISDS, List<FILES>> MapFilesToSDSS(IEnumerable<FILES> Files)
		{
			var list = new Dictionary<ISDS, List<FILES>>();

			foreach (var f in Files) {
				var sds = f.Item.Parent.Owner;
				if (!list.ContainsKey(sds))
					list.Add(sds, new List<FILES>());

				list[sds].Add(f);
			}

			return list;
		}

		public static void PopulateFxs()
		{
			new[] { "F_Size", "F_Downloaded" }
				.ToList()
				.ForEach(t => FormatFxs.Add(Tuple.Create(Guid.Empty, t), x => FToString.FormatSize(x ?? 0ul)));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "F_Done"), x => FToString.FormatPercent(x ?? 0f));
			FormatFxs.Add(Tuple.Create(Guid.Empty, "F_Priority"), x => FToString.FormatFilePriority(x ?? (FILE.PRIORITY)0));
		}
	}

	public partial class f_main {
		private void dtlv_files_FormatCell(object sender, FormatCellEventArgs e)
		{
			if ((TORRENT)lv_torrents.SelectedObject == null)
				return;

			if (e.ColumnIndex == dtlv_files_done.Index) {
				if (((FILES)e.Model).Item.Directory || ((FILES)e.Model).Item.DownloadedChunks == 0 || ((FILES)e.Model).Item.Size == 0)
					return;

				var downloaded = ((FILES)e.Model).Item.DownloadedChunks * ((TORRENT)lv_torrents.SelectedObject).ChunkSize;
				var done = (double)downloaded / ((FILES)e.Model).Item.Size * 100;
				done = done > 100 ? 100 : done;
				//e.SubItem.BackColor = Color.FromArgb(100 - done, 155 + done, 0);
				e.SubItem.BackColor = Color.FromArgb((int)(255 - done / 100 * 255), 255, (int)(255 - done / 100 * 255));
			}
		}

		private void dtlv_files_DoubleClick(object sender, EventArgs e)
		{
			var item = dtlv_files.SelectedObject;
			if (item == null)
				return;

			if (dtlv_files.IsExpanded(item))
				dtlv_files.Collapse(item);
			else
				dtlv_files.Expand(item);
		}

		private void transferTorrentDataToServerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			new f_transferData().ShowDialog();
		}

		private void ctxms_file_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (dtlv_files.SelectedObjects?.Count == 0) {
				l_priority.Enabled = l_downloadStrategy.Enabled = b_mediaInfo.Enabled = b_downloadFile.Enabled = false;
				Logger.Log(LOG_LEVEL.DEBUG, "No files selected");
				return;
			}

			l_priority.Enabled = l_downloadStrategy.Enabled = b_mediaInfo.Enabled = b_downloadFile.Enabled = true;

			Debug.Assert(dtlv_files.SelectedObjects != null);
			var files = dtlv_files.SelectedObjects.Cast<FILES>().Select(x => x.Item).Where(x => !x.Directory);
			var filesEnumer = files as FILE[] ?? files.ToArray();

			if (filesEnumer.Length == 0) {
				Logger.Log(LOG_LEVEL.DEBUG, "Torrent has no files");
				return;
			}

			b_setPriority_normal.Enabled = filesEnumer.Any(x => x.Priority != FILE.PRIORITY.NORMAL);
			b_setPriority_high.Enabled = filesEnumer.Any(x => x.Priority != FILE.PRIORITY.HIGH);
			b_setPriority_dontDownload.Enabled = filesEnumer.Any(x => x.Priority != FILE.PRIORITY.DONT_DOWNLOAD);

			b_setDownloadStretegy_normal.Enabled = filesEnumer.Any(x => x.DownloadStrategy != FILE.DOWNLOAD_STRATEGY.NORMAL);
			b_setDownloadStrategy_leading.Enabled = filesEnumer.Any(x => x.DownloadStrategy != FILE.DOWNLOAD_STRATEGY.LEADING_CHUNK_FIRST);
			b_setDownloadStrategy_trailing.Enabled = filesEnumer.Any(x => x.DownloadStrategy != FILE.DOWNLOAD_STRATEGY.TRAILING_CHUCK_FIRST);

			var sdss = File.MapFilesToSDSS(filesEnumer);
			var ops = sdss.Keys.Select(x => x.GetSupportedOperations());
			var opsEnumer = ops as SupportedOperations[] ?? ops.ToArray();

			if (!opsEnumer.All(x => x.files.DownloadFile) || dtlv_files.SelectedObjects.Count != 1) b_downloadFile.Enabled = false;
			if (!opsEnumer.All(x => x.files.MediaInfo)) b_mediaInfo.Enabled = false;
			if (!opsEnumer.All(x => x.files.SetPriority)) l_priority.Enabled = false;
			if (!opsEnumer.All(x => x.files.SetDownloadStrategy)) l_downloadStrategy.Enabled = false;
		}

		async Task FileAction<T>(Func<ISDS, FILES[], T, Task> Fx, Func<IEnumerable<FILES>, bool> FxCheck, T In)
		{
			if (dtlv_files.SelectedObjects == null) {
				Logger.Log(LOG_LEVEL.DEBUG, "No files selected");
				return;
			}

			var files = dtlv_files.SelectedObjects.Cast<FILES>();
			var filesEnumer = files as FILES[] ?? files.ToArray();

			if (FxCheck != null && !FxCheck(filesEnumer))
				return;

			var sdss = File.MapFilesToSDSS(filesEnumer);

			foreach (var kv in sdss) {
				try {
					await Fx(kv.Key, kv.Value.ToArray(), In);
				} catch (Exception ex) {
					Logger.Log(LOG_LEVEL.ERROR, "Failed to execute file action");
					Logger.LogException(LOG_LEVEL.ERROR, ex);
					return;
				}
				await File.FetchFiles(kv.Value.Select(x => x.Item.Parent).ToList());
			}
		}

		async Task SetFilePriority(FILE.PRIORITY In)
		{
			await FileAction(async (sds, files, i) => {
				var fi = files.Select(x => x.Item).Where(x => !x.Directory);
				await sds.SetFilePriority(fi, (FILE.PRIORITY)i);
				foreach (var f in fi)
					f.Priority = (FILE.PRIORITY)i;
			}, null, (int)In);
		}

		async Task SetFileDownloadStrategy(FILE.DOWNLOAD_STRATEGY In)
		{
			await FileAction(async (sds, files, i) => {
				var fi = files.Select(x => x.Item).Where(x => !x.Directory);
				await sds.SetFileDownloadStrategy(fi, (FILE.DOWNLOAD_STRATEGY)i);
				foreach (var f in fi)
					f.DownloadStrategy = (FILE.DOWNLOAD_STRATEGY)i;
			}, null, (int)In);
		}

		private async void b_setPriority_high_Click(object sender, EventArgs e) => await SetFilePriority(FILE.PRIORITY.HIGH);

		private async void b_setPriority_normal_Click(object sender, EventArgs e) => await SetFilePriority(FILE.PRIORITY.NORMAL);

		private async void b_setPriority_dontDownload_Click(object sender, EventArgs e) => await SetFilePriority(FILE.PRIORITY.DONT_DOWNLOAD);

		private async void b_setDownloadStretegy_normal_Click(object sender, EventArgs e) => await SetFileDownloadStrategy(FILE.DOWNLOAD_STRATEGY.NORMAL);

		private async void b_setDownloadStrategy_leading_Click(object sender, EventArgs e) => await SetFileDownloadStrategy(FILE.DOWNLOAD_STRATEGY.LEADING_CHUNK_FIRST);

		private async void b_setDownloadStrategy_trailing_Click(object sender, EventArgs e) => await SetFileDownloadStrategy(FILE.DOWNLOAD_STRATEGY.TRAILING_CHUCK_FIRST);

		private async void b_mediaInfo_Click(object sender, EventArgs e)
		{
			await FileAction<object>(async (sds, files, i) => {
				await sds.FileMediaInfo(files.Select(x => x.Item).Where(x => !x.Directory));
			}, null, null);
		}

		private async void b_downloadFile_Click(object sender, EventArgs e)
		{
			var fbd = new FolderBrowserDialog();

			if (fbd.ShowDialog() == DialogResult.OK) {
				await FileAction(async (sds, files, path) => {

					Func<string, string> sanitize = (inp) => {
						return Path.GetInvalidFileNameChars().Aggregate(inp, (current, c) => current.Replace(c.ToString(), ""));
					};

					Func<FILES, string, List<Tuple<FILES, string>>> dirFiles = null;
					dirFiles = (dir, basePath) => {
						Debug.Assert(dir.Item.Directory);

						Directory.CreateDirectory(basePath);

						List<Tuple<FILES, string>> ret = new List<Tuple<FILES, string>>();

						foreach (var c in dir.Children) {
							var f = files.First().Item.Parent.FileList.Where(x => x.ArrayIndex == c).First();
							if (f.Item.Directory)
								ret.AddRange(dirFiles(f, Path.Combine(basePath, sanitize(f.Item.Name))));
							else
								ret.Add(Tuple.Create(f, Path.Combine(basePath, sanitize(f.Item.Name))));
						}
						
						return ret;
					};

					List<Task> wT = new List<Task>();
					List<Tuple<FILES, string>> allFiles = new List<Tuple<FILES, string>>();

					foreach (var f in files) {
						if (f.Item.Directory) {
							var df = dirFiles(f, Path.Combine(path, sanitize(f.Item.Name)));
							allFiles.AddRange(df);
						} else
							allFiles.Add(Tuple.Create(f, Path.Combine(path, sanitize(f.Item.Name))));
					}

					allFiles = allFiles.Distinct().ToList();

					var mup = new TransferQueue();

					await mup.Multi(
						Enumerable.Repeat(sds, allFiles.Count).ToList(),
						null,
						Enumerable.Repeat(path, allFiles.Count).ToList(),
						Enumerable.Repeat(files[0].Item.Parent.RemotePath, allFiles.Count).ToList(),
						allFiles.Select(x => (object)x).ToList(),
						async (p, total, single) => {
							var file = (Tuple<FILES, string>)single;
							await sds.DownloadFile(file.Item1.Item, file.Item2, p, total);
						}
					);
				}, (files) => {
					return true;
				}, fbd.SelectedPath);
			}
		}
	}
}
