﻿using RTSharpIFace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace RTSharp
{

	[RTSPluginBridge]
	class RTSharpUIExports : IRTSharpUIExports
	{
		public object SelectedTorrentsLock => Global.SelectedTorrentsLock;

		public SynchronizationContext SyncCtxSTA => Program.MainForm.syncCtxSTA;

		public void Log(LOG_LEVEL Level, string Message) => Logger.Log(Level, Message);

		public void LogException(LOG_LEVEL Level, Exception Ex) => Logger.LogException(Level, Ex);
	}
}
