﻿using RTSharpIFace;
using System;
using System.Linq;
using System.Windows.Forms;

namespace RTSharp
{
	public partial class f_editTorrent : Form
	{
		bool ok;

		public f_editTorrent(TORRENT In)
		{
			InitializeComponent();
			Text = "Editing " + In.Name + "...";
			t_trackers.Text = String.Join(Environment.NewLine, In.Trackers.Select(x => x.Uri.OriginalString));
			t_comment.Text = In.Comment;
		}

		public bool ShowDlg()
		{
			ShowDialog();

			return ok;
		}

		private void b_cancel_Click(object sender, EventArgs e)
		{
			ok = false;
			Close();
		}

		private void b_ok_Click(object sender, EventArgs e)
		{
			ok = true;
			Close();
		}
	}
}
