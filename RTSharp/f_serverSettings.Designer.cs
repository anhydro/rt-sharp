﻿using BrightIdeasSoftware;

namespace RTSharp
{
	partial class f_serverSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_serverSettings));
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.tv_settings = new System.Windows.Forms.TreeView();
			this.cb_value = new System.Windows.Forms.ComboBox();
			this.num_value = new System.Windows.Forms.NumericUpDown();
			this.t_value = new System.Windows.Forms.TextBox();
			this.b_saveSettings = new System.Windows.Forms.Button();
			this.l_defaultValue = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.l_desc = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.l_title = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.num_value)).BeginInit();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.tv_settings);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.cb_value);
			this.splitContainer1.Panel2.Controls.Add(this.num_value);
			this.splitContainer1.Panel2.Controls.Add(this.t_value);
			this.splitContainer1.Panel2.Controls.Add(this.b_saveSettings);
			this.splitContainer1.Panel2.Controls.Add(this.l_defaultValue);
			this.splitContainer1.Panel2.Controls.Add(this.label4);
			this.splitContainer1.Panel2.Controls.Add(this.l_desc);
			this.splitContainer1.Panel2.Controls.Add(this.label3);
			this.splitContainer1.Panel2.Controls.Add(this.l_title);
			this.splitContainer1.Size = new System.Drawing.Size(606, 245);
			this.splitContainer1.SplitterDistance = 202;
			this.splitContainer1.TabIndex = 1;
			// 
			// tv_settings
			// 
			this.tv_settings.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tv_settings.Location = new System.Drawing.Point(0, 0);
			this.tv_settings.Name = "tv_settings";
			this.tv_settings.Size = new System.Drawing.Size(202, 245);
			this.tv_settings.TabIndex = 0;
			this.tv_settings.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.tv_settings_BeforeSelect);
			this.tv_settings.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_settings_AfterSelect);
			// 
			// cb_value
			// 
			this.cb_value.Dock = System.Windows.Forms.DockStyle.Top;
			this.cb_value.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cb_value.FormattingEnabled = true;
			this.cb_value.Location = new System.Drawing.Point(0, 131);
			this.cb_value.Name = "cb_value";
			this.cb_value.Size = new System.Drawing.Size(400, 21);
			this.cb_value.TabIndex = 8;
			this.cb_value.Visible = false;
			// 
			// num_value
			// 
			this.num_value.Dock = System.Windows.Forms.DockStyle.Top;
			this.num_value.Location = new System.Drawing.Point(0, 111);
			this.num_value.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
			this.num_value.Minimum = new decimal(new int[] {
            -1,
            -1,
            -1,
            -2147483648});
			this.num_value.Name = "num_value";
			this.num_value.Size = new System.Drawing.Size(400, 20);
			this.num_value.TabIndex = 7;
			this.num_value.Visible = false;
			// 
			// t_value
			// 
			this.t_value.Dock = System.Windows.Forms.DockStyle.Top;
			this.t_value.Location = new System.Drawing.Point(0, 91);
			this.t_value.Name = "t_value";
			this.t_value.Size = new System.Drawing.Size(400, 20);
			this.t_value.TabIndex = 6;
			this.t_value.Visible = false;
			// 
			// b_saveSettings
			// 
			this.b_saveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_saveSettings.Location = new System.Drawing.Point(305, 210);
			this.b_saveSettings.Name = "b_saveSettings";
			this.b_saveSettings.Size = new System.Drawing.Size(83, 23);
			this.b_saveSettings.TabIndex = 5;
			this.b_saveSettings.Text = "Save settings";
			this.b_saveSettings.UseVisualStyleBackColor = true;
			this.b_saveSettings.Click += new System.EventHandler(this.b_saveSettings_Click);
			// 
			// l_defaultValue
			// 
			this.l_defaultValue.AutoSize = true;
			this.l_defaultValue.Dock = System.Windows.Forms.DockStyle.Top;
			this.l_defaultValue.Location = new System.Drawing.Point(0, 78);
			this.l_defaultValue.Name = "l_defaultValue";
			this.l_defaultValue.Size = new System.Drawing.Size(76, 13);
			this.l_defaultValue.TabIndex = 4;
			this.l_defaultValue.Text = "Default value: ";
			this.l_defaultValue.Visible = false;
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Top;
			this.label4.Location = new System.Drawing.Point(0, 57);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(400, 21);
			this.label4.TabIndex = 3;
			// 
			// l_desc
			// 
			this.l_desc.AutoSize = true;
			this.l_desc.Dock = System.Windows.Forms.DockStyle.Top;
			this.l_desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.l_desc.ForeColor = System.Drawing.SystemColors.GrayText;
			this.l_desc.Location = new System.Drawing.Point(0, 44);
			this.l_desc.Name = "l_desc";
			this.l_desc.Size = new System.Drawing.Size(60, 13);
			this.l_desc.TabIndex = 1;
			this.l_desc.Text = "Description";
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Top;
			this.label3.Location = new System.Drawing.Point(0, 23);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(400, 21);
			this.label3.TabIndex = 2;
			// 
			// l_title
			// 
			this.l_title.Dock = System.Windows.Forms.DockStyle.Top;
			this.l_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.l_title.Location = new System.Drawing.Point(0, 0);
			this.l_title.Name = "l_title";
			this.l_title.Size = new System.Drawing.Size(400, 23);
			this.l_title.TabIndex = 0;
			this.l_title.Text = "Title";
			this.l_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.l_title.Visible = false;
			// 
			// f_serverSettings
			// 
			this.AcceptButton = this.b_saveSettings;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(606, 245);
			this.Controls.Add(this.splitContainer1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "f_serverSettings";
			this.Text = "Server settings";
			this.Load += new System.EventHandler(this.f_serverSettings_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.num_value)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TextBox t_value;
		private System.Windows.Forms.Button b_saveSettings;
		private System.Windows.Forms.Label l_defaultValue;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label l_desc;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label l_title;
		private System.Windows.Forms.TreeView tv_settings;
		private System.Windows.Forms.ComboBox cb_value;
		private System.Windows.Forms.NumericUpDown num_value;
	}
}