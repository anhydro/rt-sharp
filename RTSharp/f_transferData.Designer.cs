﻿namespace RTSharp
{
	partial class f_transferData
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_transferData));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cmb_sdss = new System.Windows.Forms.ComboBox();
			this.g_folder = new System.Windows.Forms.GroupBox();
			this.t_folder = new System.Windows.Forms.TextBox();
			this.b_browse_folder = new System.Windows.Forms.Button();
			this.rb_folder = new System.Windows.Forms.RadioButton();
			this.g_file = new System.Windows.Forms.GroupBox();
			this.t_file = new System.Windows.Forms.TextBox();
			this.b_browser_file = new System.Windows.Forms.Button();
			this.rb_file = new System.Windows.Forms.RadioButton();
			this.b_transfer = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.b_remoteBrowse = new System.Windows.Forms.Button();
			this.t_remoteDest = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.g_folder.SuspendLayout();
			this.g_file.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cmb_sdss);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(479, 46);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Connection";
			// 
			// cmb_sdss
			// 
			this.cmb_sdss.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cmb_sdss.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmb_sdss.FormattingEnabled = true;
			this.cmb_sdss.Location = new System.Drawing.Point(6, 19);
			this.cmb_sdss.Name = "cmb_sdss";
			this.cmb_sdss.Size = new System.Drawing.Size(467, 21);
			this.cmb_sdss.TabIndex = 0;
			this.cmb_sdss.SelectedIndexChanged += new System.EventHandler(this.cmb_sdss_SelectedIndexChanged);
			// 
			// g_folder
			// 
			this.g_folder.Controls.Add(this.t_folder);
			this.g_folder.Controls.Add(this.b_browse_folder);
			this.g_folder.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_folder.Location = new System.Drawing.Point(0, 46);
			this.g_folder.Name = "g_folder";
			this.g_folder.Size = new System.Drawing.Size(479, 45);
			this.g_folder.TabIndex = 1;
			this.g_folder.TabStop = false;
			this.g_folder.Text = "    Folder";
			this.g_folder.MouseClick += new System.Windows.Forms.MouseEventHandler(this.g_folder_MouseClick);
			// 
			// t_folder
			// 
			this.t_folder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_folder.Location = new System.Drawing.Point(6, 19);
			this.t_folder.Name = "t_folder";
			this.t_folder.Size = new System.Drawing.Size(386, 20);
			this.t_folder.TabIndex = 0;
			this.t_folder.MouseClick += new System.Windows.Forms.MouseEventHandler(this.t_folder_MouseClick);
			// 
			// b_browse_folder
			// 
			this.b_browse_folder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_browse_folder.Location = new System.Drawing.Point(398, 17);
			this.b_browse_folder.Name = "b_browse_folder";
			this.b_browse_folder.Size = new System.Drawing.Size(75, 23);
			this.b_browse_folder.TabIndex = 2;
			this.b_browse_folder.Text = "Browse...";
			this.b_browse_folder.UseVisualStyleBackColor = true;
			this.b_browse_folder.Click += new System.EventHandler(this.b_browse_folder_Click);
			// 
			// rb_folder
			// 
			this.rb_folder.AutoSize = true;
			this.rb_folder.Location = new System.Drawing.Point(5, 47);
			this.rb_folder.Name = "rb_folder";
			this.rb_folder.Size = new System.Drawing.Size(14, 13);
			this.rb_folder.TabIndex = 4;
			this.rb_folder.TabStop = true;
			this.rb_folder.UseVisualStyleBackColor = true;
			// 
			// g_file
			// 
			this.g_file.Controls.Add(this.t_file);
			this.g_file.Controls.Add(this.b_browser_file);
			this.g_file.Dock = System.Windows.Forms.DockStyle.Top;
			this.g_file.Location = new System.Drawing.Point(0, 91);
			this.g_file.Name = "g_file";
			this.g_file.Size = new System.Drawing.Size(479, 45);
			this.g_file.TabIndex = 0;
			this.g_file.TabStop = false;
			this.g_file.Text = "    File";
			this.g_file.MouseClick += new System.Windows.Forms.MouseEventHandler(this.g_file_MouseClick);
			// 
			// t_file
			// 
			this.t_file.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_file.Location = new System.Drawing.Point(6, 19);
			this.t_file.Name = "t_file";
			this.t_file.Size = new System.Drawing.Size(386, 20);
			this.t_file.TabIndex = 1;
			this.t_file.MouseClick += new System.Windows.Forms.MouseEventHandler(this.t_file_MouseClick);
			// 
			// b_browser_file
			// 
			this.b_browser_file.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_browser_file.Location = new System.Drawing.Point(398, 17);
			this.b_browser_file.Name = "b_browser_file";
			this.b_browser_file.Size = new System.Drawing.Size(75, 23);
			this.b_browser_file.TabIndex = 3;
			this.b_browser_file.Text = "Browse...";
			this.b_browser_file.UseVisualStyleBackColor = true;
			this.b_browser_file.Click += new System.EventHandler(this.b_browser_file_Click);
			// 
			// rb_file
			// 
			this.rb_file.AutoSize = true;
			this.rb_file.Location = new System.Drawing.Point(5, 92);
			this.rb_file.Name = "rb_file";
			this.rb_file.Size = new System.Drawing.Size(14, 13);
			this.rb_file.TabIndex = 5;
			this.rb_file.TabStop = true;
			this.rb_file.UseVisualStyleBackColor = true;
			// 
			// b_transfer
			// 
			this.b_transfer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.b_transfer.Location = new System.Drawing.Point(6, 202);
			this.b_transfer.Name = "b_transfer";
			this.b_transfer.Size = new System.Drawing.Size(461, 23);
			this.b_transfer.TabIndex = 2;
			this.b_transfer.Text = "Transfer";
			this.b_transfer.UseVisualStyleBackColor = true;
			this.b_transfer.Click += new System.EventHandler(this.b_transfer_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.b_remoteBrowse);
			this.groupBox2.Controls.Add(this.t_remoteDest);
			this.groupBox2.Location = new System.Drawing.Point(0, 142);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(479, 45);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Remote destination";
			// 
			// b_remoteBrowse
			// 
			this.b_remoteBrowse.Location = new System.Drawing.Point(398, 17);
			this.b_remoteBrowse.Name = "b_remoteBrowse";
			this.b_remoteBrowse.Size = new System.Drawing.Size(75, 23);
			this.b_remoteBrowse.TabIndex = 1;
			this.b_remoteBrowse.Text = "Browse...";
			this.b_remoteBrowse.UseVisualStyleBackColor = true;
			this.b_remoteBrowse.Click += new System.EventHandler(this.b_remoteBrowse_Click);
			// 
			// t_remoteDest
			// 
			this.t_remoteDest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_remoteDest.Location = new System.Drawing.Point(6, 19);
			this.t_remoteDest.Name = "t_remoteDest";
			this.t_remoteDest.Size = new System.Drawing.Size(386, 20);
			this.t_remoteDest.TabIndex = 0;
			// 
			// f_transferData
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(479, 237);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.rb_file);
			this.Controls.Add(this.rb_folder);
			this.Controls.Add(this.b_transfer);
			this.Controls.Add(this.g_file);
			this.Controls.Add(this.g_folder);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(2147483408, 276);
			this.MinimumSize = new System.Drawing.Size(16, 216);
			this.Name = "f_transferData";
			this.Text = "Transfer data";
			this.Load += new System.EventHandler(this.f_transferData_Load);
			this.groupBox1.ResumeLayout(false);
			this.g_folder.ResumeLayout(false);
			this.g_folder.PerformLayout();
			this.g_file.ResumeLayout(false);
			this.g_file.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox cmb_sdss;
		private System.Windows.Forms.GroupBox g_folder;
		private System.Windows.Forms.TextBox t_folder;
		private System.Windows.Forms.Button b_browse_folder;
		private System.Windows.Forms.GroupBox g_file;
		private System.Windows.Forms.TextBox t_file;
		private System.Windows.Forms.Button b_browser_file;
		private System.Windows.Forms.Button b_transfer;
		private System.Windows.Forms.RadioButton rb_folder;
		private System.Windows.Forms.RadioButton rb_file;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button b_remoteBrowse;
		private System.Windows.Forms.TextBox t_remoteDest;
	}
}