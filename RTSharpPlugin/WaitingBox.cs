﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharpIFace {
	public partial class WaitingBox : Form {

		bool NoClose = true;
		public bool ForceClose { get; set; }

		private const int CP_NOCLOSE_BUTTON = 0x200;
		protected override CreateParams CreateParams {
			get {
				CreateParams myCp = base.CreateParams;
				if (NoClose)
					myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
				return myCp;
			}
		}

		public WaitingBox(string Text, ProgressBarStyle SingleStyle, ProgressBarStyle AllStyle, bool NoClose = true) {
			this.NoClose = NoClose;

			if (!NoClose)
				TopMost = false;

			InitializeComponent();
			l_status.Text = Text;
			pbar_single.Style = SingleStyle;
			pbar_all.Style = AllStyle;
		}

		public void UpdateText(string Text) {
			l_status.Text = Text;
		}

		public void UpdatePBarSingle(byte Value) {
			pbar_single.Value = Value;
		}

		public void UpdatePBarAll(byte Value) {
			pbar_all.Value = Value;
		}

		public void UpdatePBarStyle(ProgressBarStyle SingleStyle, ProgressBarStyle AllStyle) {
			pbar_single.Style = SingleStyle;
			pbar_all.Style = AllStyle;
		}

		private void WaitingBox_FormClosing(object sender, FormClosingEventArgs e)
		{
			ForceClose = true;
		}
	}
}
