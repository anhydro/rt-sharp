﻿using BrightIdeasSoftware;

namespace RTSharpIFace
{
	partial class SelectRemoteFolder
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectRemoteFolder));
			this.b_ok = new System.Windows.Forms.Button();
			this.b_cancel = new System.Windows.Forms.Button();
			this.b_newFolder = new System.Windows.Forms.Button();
			this.tlv_dirs = new BrightIdeasSoftware.TreeListView();
			this.col_main = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
			((System.ComponentModel.ISupportInitialize)(this.tlv_dirs)).BeginInit();
			this.SuspendLayout();
			// 
			// b_ok
			// 
			this.b_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_ok.Location = new System.Drawing.Point(214, 412);
			this.b_ok.Name = "b_ok";
			this.b_ok.Size = new System.Drawing.Size(75, 23);
			this.b_ok.TabIndex = 0;
			this.b_ok.Text = "OK";
			this.b_ok.UseVisualStyleBackColor = true;
			this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
			// 
			// b_cancel
			// 
			this.b_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_cancel.Location = new System.Drawing.Point(295, 412);
			this.b_cancel.Name = "b_cancel";
			this.b_cancel.Size = new System.Drawing.Size(75, 23);
			this.b_cancel.TabIndex = 1;
			this.b_cancel.Text = "Cancel";
			this.b_cancel.UseVisualStyleBackColor = true;
			this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
			// 
			// b_newFolder
			// 
			this.b_newFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.b_newFolder.Location = new System.Drawing.Point(12, 412);
			this.b_newFolder.Name = "b_newFolder";
			this.b_newFolder.Size = new System.Drawing.Size(103, 23);
			this.b_newFolder.TabIndex = 2;
			this.b_newFolder.Text = "Make New Folder";
			this.b_newFolder.UseVisualStyleBackColor = true;
			this.b_newFolder.Click += new System.EventHandler(this.b_newFolder_Click);
			// 
			// tlv_dirs
			// 
			this.tlv_dirs.AllColumns.Add(this.col_main);
			this.tlv_dirs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tlv_dirs.CellEditUseWholeCell = false;
			this.tlv_dirs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_main});
			this.tlv_dirs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tlv_dirs.HideSelection = false;
			this.tlv_dirs.Location = new System.Drawing.Point(12, 12);
			this.tlv_dirs.Name = "tlv_dirs";
			this.tlv_dirs.ShowGroups = false;
			this.tlv_dirs.Size = new System.Drawing.Size(358, 374);
			this.tlv_dirs.TabIndex = 3;
			this.tlv_dirs.UseCompatibleStateImageBehavior = false;
			this.tlv_dirs.View = System.Windows.Forms.View.Details;
			this.tlv_dirs.VirtualMode = true;
			this.tlv_dirs.SelectedIndexChanged += new System.EventHandler(this.tlv_dirs_SelectedIndexChanged);
			// 
			// col_main
			// 
			this.col_main.FillsFreeSpace = true;
			this.col_main.Text = "";
			// 
			// SelectRemoteFolder
			// 
			this.AcceptButton = this.b_ok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(382, 447);
			this.Controls.Add(this.tlv_dirs);
			this.Controls.Add(this.b_newFolder);
			this.Controls.Add(this.b_cancel);
			this.Controls.Add(this.b_ok);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SelectRemoteFolder";
			this.Text = "Browse For Folder";
			this.Load += new System.EventHandler(this.SelectRemoteFolder_Load);
			((System.ComponentModel.ISupportInitialize)(this.tlv_dirs)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button b_ok;
		private System.Windows.Forms.Button b_cancel;
		private System.Windows.Forms.Button b_newFolder;
		private TreeListView tlv_dirs;
		private OLVColumn col_main;
	}
}