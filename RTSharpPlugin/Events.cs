﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSharpIFace
{
	public class RTSEvents
	{
		public class TorrentStateArgs : EventArgs
		{
			public readonly TORRENT_STATE State;

			public TorrentStateArgs(TORRENT_STATE State)
			{
				this.State = State;
			}
		}

		public class NewTorrentArgs : EventArgs
		{
			public readonly TORRENT Torrent;

			public NewTorrentArgs(TORRENT Torrent)
			{
				this.Torrent = Torrent;
			}
		}

		public class TorrentRemovedArgs : EventArgs
		{
			public readonly byte[] Hash;

			public TorrentRemovedArgs(byte[] Hash)
			{
				this.Hash = Hash;
			}
		}

		public class TorrentFinishedArgs : EventArgs
		{
			public readonly TORRENT_STATE State;

			public TorrentFinishedArgs(TORRENT_STATE State)
			{
				this.State = State;
			}
		}

		public class MagnetResolvedArgs : EventArgs
		{
			public readonly TORRENT Torrent;

			public MagnetResolvedArgs(TORRENT Torrent)
			{
				this.Torrent = Torrent;
			}
		}

		public static event EventHandler<TorrentStateArgs> TorrentStateChange;
		public static event EventHandler<NewTorrentArgs> NewTorrent;
		public static event EventHandler<TorrentRemovedArgs> TorrentRemoved;
		public static event EventHandler<TorrentFinishedArgs> TorrentFinished;
		public static event EventHandler<MagnetResolvedArgs> MagnetResolved;

		static internal class Dispatcher
		{
			internal static void TorrentStateChangedEH(TORRENT sender, TorrentStateArgs e) => TorrentStateChange?.Invoke(sender, e);
			internal static void NewTorrentAddedEH(ISDS sender, NewTorrentArgs e) => NewTorrent?.Invoke(sender, e);
			internal static void TorrentRemovedEH(ISDS sender, TorrentRemovedArgs e) => TorrentRemoved?.Invoke(sender, e);
			internal static void TorrentFinishedEH(TORRENT sender, TorrentFinishedArgs e) => TorrentFinished?.Invoke(sender, e);
			internal static void MagnetResolvedEH(MagnetResolvedArgs e) => MagnetResolved?.Invoke(null, e);
		}
	}
}
