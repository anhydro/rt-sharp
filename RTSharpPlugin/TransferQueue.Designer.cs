﻿namespace RTSharpIFace
{
	partial class TransferQueue
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferQueue));
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.l_dlSpeed = new System.Windows.Forms.ToolStripStatusLabel();
			this.l_upSpeed = new System.Windows.Forms.ToolStripStatusLabel();
			this.lv_downloads = new System.Windows.Forms.ListView();
			this.lch_source = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lch_target = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lch_sourcePath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lch_destinationPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lch_progress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tmr_globalSpeed = new System.Windows.Forms.Timer(this.components);
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.l_dlSpeed,
			this.l_upSpeed});
			this.statusStrip1.Location = new System.Drawing.Point(0, 264);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(766, 22);
			this.statusStrip1.TabIndex = 0;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// l_dlSpeed
			// 
			this.l_dlSpeed.AutoSize = false;
			this.l_dlSpeed.Name = "l_dlSpeed";
			this.l_dlSpeed.Size = new System.Drawing.Size(150, 17);
			this.l_dlSpeed.Text = "DL Speed:";
			this.l_dlSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// l_upSpeed
			// 
			this.l_upSpeed.AutoSize = false;
			this.l_upSpeed.Name = "l_upSpeed";
			this.l_upSpeed.Size = new System.Drawing.Size(150, 17);
			this.l_upSpeed.Text = "UP Speed:";
			this.l_upSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lv_downloads
			// 
			this.lv_downloads.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
			this.lch_source,
			this.lch_target,
			this.lch_sourcePath,
			this.lch_destinationPath,
			this.lch_progress});
			this.lv_downloads.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_downloads.FullRowSelect = true;
			this.lv_downloads.GridLines = true;
			this.lv_downloads.HideSelection = false;
			this.lv_downloads.Location = new System.Drawing.Point(0, 0);
			this.lv_downloads.MultiSelect = false;
			this.lv_downloads.Name = "lv_downloads";
			this.lv_downloads.ShowGroups = false;
			this.lv_downloads.Size = new System.Drawing.Size(766, 264);
			this.lv_downloads.TabIndex = 1;
			this.lv_downloads.UseCompatibleStateImageBehavior = false;
			this.lv_downloads.View = System.Windows.Forms.View.Details;
			// 
			// lch_source
			// 
			this.lch_source.Text = "Source";
			// 
			// lch_target
			// 
			this.lch_target.Text = "Target";
			// 
			// lch_sourcePath
			// 
			this.lch_sourcePath.Text = "Source path";
			this.lch_sourcePath.Width = 200;
			// 
			// lch_destinationPath
			// 
			this.lch_destinationPath.Text = "Destination path";
			this.lch_destinationPath.Width = 200;
			// 
			// lch_progress
			// 
			this.lch_progress.Text = "Progress";
			this.lch_progress.Width = 227;
			// 
			// tmr_globalSpeed
			// 
			this.tmr_globalSpeed.Enabled = true;
			this.tmr_globalSpeed.Interval = 1000;
			this.tmr_globalSpeed.Tick += new System.EventHandler(this.tmr_globalSpeed_Tick);
			// 
			// TransferQueue
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(766, 286);
			this.Controls.Add(this.lv_downloads);
			this.Controls.Add(this.statusStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "TransferQueue";
			this.Text = "Transfer queue";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TransferQueue_FormClosing);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel l_dlSpeed;
		private System.Windows.Forms.ToolStripStatusLabel l_upSpeed;
		private System.Windows.Forms.ListView lv_downloads;
		private System.Windows.Forms.ColumnHeader lch_source;
		private System.Windows.Forms.ColumnHeader lch_target;
		private System.Windows.Forms.ColumnHeader lch_sourcePath;
		private System.Windows.Forms.ColumnHeader lch_destinationPath;
		private System.Windows.Forms.ColumnHeader lch_progress;
		private System.Windows.Forms.Timer tmr_globalSpeed;
	}
}