﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace RTSharpIFace {
	public partial class ResultsBox : Form {
		public ResultsBox(bool MonospaceFont = false) {
			InitializeComponent();
			if (MonospaceFont)
				t_text.Font = new Font(FontFamily.GenericMonospace, t_text.Font.Size);
		}

		public void Append(string Text)
		{
			t_text.Text += Text + Environment.NewLine;
		}

		public void Show(string Text) {
			t_text.Text = Text;
			Show();
		}
	}
}
