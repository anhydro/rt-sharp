﻿namespace RTSharpIFace
{
	public class ISFA
	{
		public struct TOKEN
		{
			public readonly byte[] Token;
			public readonly string Host;
			public readonly ushort Port;
			public readonly string Path;
			public ISDS Source;
			public readonly ISDS Target;
			public readonly bool SSL;
			public readonly byte[] SSLHash;

			public TOKEN(ISDS Source, ISDS Target, byte[] Token, string Host, ushort Port, string Path, bool SSL, byte[] SSLHash)
			{
				this.Token = Token;
				this.Host = Host;
				this.Port = Port;
				this.Source = Source;
				this.Target = Target;
				this.Path = Path;
				this.SSL = SSL;
				this.SSLHash = SSLHash;
			}
		}
	}
}
