﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace RTSharpIFace
{
	public static class Settings
	{
		/// <summary>
		/// Torrent listing poll interval
		/// </summary>
		public static int RefreshInterval = -1;

		/// <summary>
		/// Prefered user date format
		/// </summary>
		/// <seealso cref="DateTime.ToString(string)"/>
		public static string DateFormat;

		/// <summary>
		/// Dictionary of tracker url -> user-friendly name aliases. Tracker can be only matched with an alias when <c>Key</c> is compared with tracker url using <see cref="String.Contains(string)"/>
		/// </summary>
		/// <seealso cref="TORRENT.TrackerSingle"/>
		/// <seealso cref="TORRENT.Trackers"/>
		public static Dictionary<string, string> TrackerMatches = null;

		/// <summary>
		/// Dictionary of domains returned by peer IP WhoIS -> transformed domain strings. Domain can be only matched with an replacement when <c>Key</c> is compared with domain using <see cref="String.Equals(string)"/> (or operator <c>==</c>)
		/// </summary>
		/// <seealso cref="PEER.Domain"/>
		public static Dictionary<string, string> DomainReplacements = null;

		/// <summary>
		/// Loop iteration limit when calculating folder sizes recursively.
		/// </summary>
		/// <seealso cref="FILES"/>
		/// <seealso cref="FILE"/>
		/// <seealso cref="ISDS.GetTorrentFiles(List{TORRENT})"/>
		public static int FolderParseLimit = -1;

		/// <summary>
		/// Map of colors to SDSs (unique). This is used for highlighting rows of torrents that are part of one or another SDS.
		/// <seealso cref="IPlugin.UniqueGUID"/>
		/// </summary>
		public static Dictionary<Guid, Color> SDSHighlighting;

		/// <summary>
		/// Enable saving torrents into a zip file
		/// </summary>
		public static bool SaveTorrentsInZip;

		/// <summary>
		/// Points limit of graphs
		/// </summary>
		public static int GraphPointsLimit = -1;
	}
}
