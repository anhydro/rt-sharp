﻿using System;
using System.Collections.Generic;

namespace RTSharpIFace
{
	/// <summary>
	/// Failure to configure plugin
	/// </summary>
	public class ConfigException : Exception
	{
		/// <summary>
		/// ctor
		/// </summary>
		public ConfigException()
		{
		}

		/// <summary>
		/// ctor
		/// </summary>
		/// <param name="message">Message</param>
		/// <param name="ConfigPartAt">Configuration part, where exception occurred</param>
		public ConfigException(string message, string ConfigPartAt) : base(message)
		{
			message = message + " at " + ConfigPartAt;
		}
	}

	/// <summary>
	/// SDS comparer based on unique GUID
	/// </summary>
	public class UniqSDSComparer : IEqualityComparer<ISDS>
	{
		public int GetHashCode(ISDS sds) { return sds.UniqueGUID.GetHashCode(); }
		public bool Equals(ISDS a, ISDS b) { return a.UniqueGUID.Equals(b.UniqueGUID); }
	}

	/// <summary>
	/// SDS comparer based on GUID
	/// </summary>
	public class SDSComparer : IEqualityComparer<ISDS>
	{
		public int GetHashCode(ISDS sds) { return sds.GUID.GetHashCode(); }
		public bool Equals(ISDS a, ISDS b) { return a.GUID.Equals(b.GUID); }
	}
}
