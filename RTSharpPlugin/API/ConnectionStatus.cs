﻿namespace RTSharpIFace
{
	/// <summary>
	/// Connection status
	/// </summary>
	public class ConnectionStatus
	{
		/// <summary>
		/// Connection status
		/// </summary>
		public bool Status { get; set; }

		/// <summary>
		/// Self
		/// </summary>
		public ISDS Owner { get; set; }

		/// <summary>
		/// Connection title, usually SDS friendly name but longer :)
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Optional, can be multi-line, usually includes remote server free disk space, etc.
		/// </summary>
		public string Description { get; set; }

		/// <param name="Self"><see cref="Owner"/></param>
		/// <param name="Status"><see cref="Status"/></param>
		/// <param name="Title"><see cref="Title"/></param>
		/// <param name="Description"><see cref="Description"/></param>
		public ConnectionStatus(ISDS Self, bool Status, string Title, string Description)
		{
			Owner = Self;
			this.Status = Status;
			this.Title = Title;
			this.Description = Description;
		}
	}
}
