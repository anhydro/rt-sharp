﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharpIFace
{
	/// <summary>
	/// Plugin
	/// </summary>
	public interface IPlugin
	{
		/// <summary>
		/// Plugin name
		/// </summary>
		string Name {
			get;
		}
		/// <summary>
		/// Additional description
		/// </summary>
		string Description {
			get;
		}
		/// <summary>
		/// Author
		/// </summary>
		string Author {
			get;
		}
		/// <summary>
		/// Version / date
		/// </summary>
		string Version {
			get;
		}

		/// <summary>
		/// Initialize plugin
		/// </summary>
		/// <param name="Host">Host object</param>
		/// <param name="UniqueGUID">Unique plugin GUID</param>
		/// <param name="WBox">Waiting box to report progess. Plugin must only update single PBar. <seealso cref="WaitingBox.UpdatePBarSingle(byte)"/></param>
		void Init(IRTSharp Host, Guid UniqueGUID, WaitingBox WBox);

		/// <summary>
		/// Unload plugin
		/// </summary>
		void Unload();

		/// <summary>
		/// Field for accessing custom functions specific for plugin type
		/// </summary>
		/// <remarks>Used to access data from plugin to plugin. See target plugin documentation for input and output formats</remarks>
		/// <param name="In">Anything and everything</param>
		/// <returns>Anything and everything</returns>
		Task<dynamic> CustomAccess(dynamic In);

		/// <summary>
		/// Form for plugin settings
		/// </summary>
		/// <param name="Self">Plugin self</param>
		/// <returns>Settings form</returns>
		Form GetPluginSettings(IPlugin Self);

		/// <summary>
		/// Unique GUID
		/// </summary>
		Guid UniqueGUID {
			get;
		}

		/// <summary>
		/// A GUID that is unique to a plugin, but not an instance of plugin.
		/// </summary>
		Guid GUID {
			get;
		}
	}
}
