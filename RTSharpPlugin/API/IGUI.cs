﻿using BrightIdeasSoftware;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace RTSharpIFace
{
	public interface IGUIDirect
	{
		Control GetCoreControl(string el);

		ToolStripItem GetCoreToolStripItemControl(string el);

		dynamic GetCoreObject(string el);

		[Obsolete("Use IRTSharp.MainForm")]
		Form GetCore();

		Action<float, OLVListSubItem> RatioColoring { get; set; }
	}

	public interface IGUI
	{
		void AddToolbarButton(ToolStripItem Item);

		IGUIDirect Direct { get; }

		/// <summary>
		/// First tuple item is foreground color, second is background color
		/// </summary>
		Func<float, Tuple<Color?, Color?>> RatioColoring { get; set; }
	}
}