﻿using System.Collections.Generic;
using BencodeNET.Torrents;
using BencodeNET.Parsing;
using System.IO;
using System.Reflection;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.Text.RegularExpressions;

namespace RTSharpIFace
{
	public class MagnetManip
	{
		private string Magnet;
		private System.Collections.Specialized.NameValueCollection Params;
		private static Regex MagnetRegex = new Regex("[a-fA-F0-9]{40}");

		public MagnetManip(string Magnet)
		{
			this.Magnet = Magnet;
			var queryString = Magnet.Substring(Magnet.IndexOf('?')).Split('#')[0];
			Params = System.Web.HttpUtility.ParseQueryString(queryString);
		}

		private byte[] XtToByteArray(string Xt)
		{
			if (MagnetRegex.IsMatch(Xt))
				return Utils.StringToByteArray(Xt);
			else
				return Base32Encoding.ToBytes(Xt);
		}

		public List<byte[]> GetBtih()
		{
			try {
				var xt = Params["xt"];

				if (!xt.StartsWith("urn:btih:"))
					throw new InvalidDataException("xt doesn't start with urn:btih:");
				xt = xt.Remove(0, 9);

				return new[] { XtToByteArray(xt) }.ToList();
			} catch {
				int x = 0;
				var ret = new List<byte[]>();

				while (Params.AllKeys.Contains("xt." + x)) {

					if (!Params["xt." + x].StartsWith("urn:btih:"))
						throw new InvalidDataException("xt." + x + " doesn't start with urn:btih:");
					Params["xt." + x] = Params["xt." + x].Remove(0, 9);

					ret.Add(XtToByteArray(Params["xt." + x]));
				}

				return ret;
			}
		}
	}
	public class TorrentManip
	{
		public Torrent Torrent;

		public TorrentManip() { }
		public TorrentManip(byte[] Raw) {
			var parser = new BencodeParser();
			Torrent = parser.Parse<Torrent>(Raw);
		}

		public TorrentManip(FileStream Fs)
		{
			var parser = new BencodeParser();
			Torrent = parser.Parse<Torrent>(Fs);
		}

		public void SetTrackers(IEnumerable<string> Trackers)
		{
			Torrent.Trackers.Clear();
			Torrent.Trackers = new List<IList<string>>();
			foreach (var t in Trackers)
				Torrent.Trackers.Add(new[] { t });
		}

		public byte[] GetInfoHash() => Torrent.GetInfoHashBytes();

		public override string ToString()
		{
			return Torrent.EncodeAsString();
		}
	}

	public class TorrentCreator
	{
		string workingPath;
		public TorrentCreator(string workingDirectory)
		{
			workingPath = workingDirectory;
		}

		private static Task WaitForExitAsync(Process process,
		CancellationToken cancellationToken = default(CancellationToken))
		{
			var tcs = new TaskCompletionSource<object>();
			process.EnableRaisingEvents = true;
			process.Exited += (sender, args) => tcs.TrySetResult(null);
			if (cancellationToken != default(CancellationToken))
				cancellationToken.Register(tcs.SetCanceled);

			return tcs.Task;
		}

		public async Task Create(Uri Announce, string InputDirectory, string Output, bool Private)
		{
			string utils = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Locati‌​on), "utils");
			utils = Path.Combine(utils, Environment.Is64BitOperatingSystem ? "x64" : "x86");

			var proc = new Process();
			proc.StartInfo.FileName = Path.Combine(utils, "mktorrent.exe");
			proc.StartInfo.Arguments =
				"-a \"" + Announce.OriginalString.Replace("\\", "\\\\").Replace("\"", "\\\"") + "\" " +
				"-o \"" + Output.Replace("\\", "\\\\").Replace("\"", "\\\"") + "\" " +
				"-b \"mktorrent 1.1\" " + 
				(Private ? "-p" : "") + 
				" \"" + Path.GetFileName(InputDirectory).Replace("\\", "\\\\").Replace("\"", "\\\"") + "\"";
			proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			proc.StartInfo.CreateNoWindow = true;
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.RedirectStandardOutput = true;
			proc.StartInfo.RedirectStandardError = true;
			proc.StartInfo.WorkingDirectory = workingPath;
			proc.Start();
			proc.BeginErrorReadLine();
			proc.BeginOutputReadLine();
			var output = new List<string>();
			proc.OutputDataReceived += (sender, e) => {
				output.Add(e.Data);
			};
			proc.ErrorDataReceived += (sender, e) => {
				output.Add(e.Data);
			};

			await WaitForExitAsync(proc);

			if (proc.ExitCode != 0)
				throw new Exception(output.Aggregate((a, b) => a + Environment.NewLine + b));
		}
	}
}
