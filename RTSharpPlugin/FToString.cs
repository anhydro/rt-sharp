﻿using System;
using System.Collections.Generic;
using static RTSharpIFace.Utils;

namespace RTSharpIFace
{
	public static class FToString
	{
		public static string FormatState(TORRENT_STATE In, TORRENT_ISFA Isfa)
		{
			string ret = "N/A";
			if ((In & TORRENT_STATE.SEEDING) == TORRENT_STATE.SEEDING)
				ret = "Seeding";
			if ((In & TORRENT_STATE.STOPPED) == TORRENT_STATE.STOPPED)
				ret = "Stopped";
			if ((In & TORRENT_STATE.COMPLETE) == TORRENT_STATE.COMPLETE)
				ret = "Complete";
			if ((In & TORRENT_STATE.DOWNLOADING) == TORRENT_STATE.DOWNLOADING)
				ret = "Downloading";
			if ((In & TORRENT_STATE.PAUSED) == TORRENT_STATE.PAUSED)
				ret = "Paused";
			if ((In & TORRENT_STATE.HASHING) == TORRENT_STATE.HASHING)
				ret = "Hashing";

			if (Isfa == TORRENT_ISFA.NONE && (In & TORRENT_STATE.ACTIVE) == TORRENT_STATE.ACTIVE)
				ret = "[⚡] " + ret;
			else if (Isfa == TORRENT_ISFA.NONE && (In & TORRENT_STATE.ERRORED) == TORRENT_STATE.ERRORED)
				ret = "[☠] " + ret;
			else {
				if (Isfa == TORRENT_ISFA.PENDING)
					ret = "[DUP…] " + ret;
				else if (Isfa == TORRENT_ISFA.EXECUTING)
					ret = "[DUP⚡] " + ret;
				else if (Isfa == TORRENT_ISFA.PROCESSING)
					ret = "[DUP➠] " + ret;
			}
				
			return ret;
		}

		public static string FormatSpeed(ulong In) => GetSizeSI(In) + "/s";

		public static string FormatSize(ulong In) => GetSizeSI(In);

		public static string FormatUnixTimeStamp(ulong In, string DateFormat) => (In == 0 ? DateTime.MinValue : UnixTimeStampToDateTime(In)).ToString(DateFormat);

		public static string FormatTracker(string In, Dictionary<string, string> TrackerMatches) => TORRENT.TrackerTransform(In);

		public static string FormatPercent(float In) => Math.Round(In, 2) + "%";

		public static string FormatRatio(float In) => In == Single.MaxValue ? "∞" : Math.Round(In, 3).ToString();

		public static string FormatAgo(float In) => In == 0 ? "" : ToAgoString(TimeSpan.FromSeconds(In));
		
		public static string FormatConnectedTotal(Tuple<uint, uint> In) => FormatConnectedTotal(In.Item1, In.Item2);
		public static string FormatConnectedTotal(uint Connected, uint Total) => Connected + " (" + Total + ")";

		public static string FormatTorrentPriority(TORRENT_PRIORITY In)
		{
			if (In == TORRENT_PRIORITY.HIGH)
				return "High";
			if (In == TORRENT_PRIORITY.LOW)
				return "Low";
			if (In == TORRENT_PRIORITY.NORMAL)
				return "Normal";
			return In == TORRENT_PRIORITY.OFF ? "Off" : "N/A";
		}

		public static string FormatFilePriority(FILE.PRIORITY In)
		{
			if (In == FILE.PRIORITY.HIGH)
				return "High";
			if (In == FILE.PRIORITY.NORMAL)
				return "Normal";
			return In == FILE.PRIORITY.DONT_DOWNLOAD ? "Don't download" : "N/A";
		}

		public static string FormatPeerFlags(PEER_FLAGS In) {
			string ret = "";
	        if ((In & PEER_FLAGS.E_ENCRYPTED) == PEER_FLAGS.E_ENCRYPTED)
	            ret += " E";
            if ((In & PEER_FLAGS.I_INCOMING) == PEER_FLAGS.I_INCOMING)
                ret += " I";
            if ((In & PEER_FLAGS.O_OBFUSCATED) == PEER_FLAGS.O_OBFUSCATED)
                ret += " O";
            if ((In & PEER_FLAGS.S_SNUBBED) == PEER_FLAGS.S_SNUBBED)
                ret += " S";

            return ret.TrimStart(' ');
		}
	}
}
