﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharpIFace
{
	public partial class SelectRemoteFolder : Form
	{
		ISDS SDS;
		public SelectRemoteFolder(ISDS SDS, string selectDir = "")
		{
			this.SDS = SDS;
			InitializeComponent();

			tlv_dirs.CanExpandGetter = x => {
				return true;
			};

			tlv_dirs.ChildrenGetter = x => {
				var path = (string)x;
				string[] res;
				try {
					res = Task.Run(() => SDS.GetRemoteDirectories(path)).Result.ToArray();
				} catch (Exception ex) {
					PluginBase.RTSharpUIExports.Log(LOG_LEVEL.ERROR, "Failed to get directory listing for " + path);
					PluginBase.RTSharpUIExports.LogException(LOG_LEVEL.ERROR, ex);
					return new string[] { };
				}
				return res.OrderBy(t => t);
			};

			tlv_dirs.Roots = new[] { "/" };

			col_main.AspectGetter = x => ((string)x)
				.Split(System.IO.Path.DirectorySeparatorChar)
				.Last()
				.Split(System.IO.Path.AltDirectorySeparatorChar)
				.Last();

			var defaultDir = selectDir == "" ? Task.Run(() => SDS.GetDefaultSavePath()).Result : selectDir;

			string curPath = "/";
			bool l = defaultDir.Contains("/");

			var curBranch = tlv_dirs.TreeModel.GetBranch("/");
			curBranch.FetchChildren();

			foreach (var comp in defaultDir.Split(l ? '/' : '\\')) {
				if (String.IsNullOrEmpty(comp))
					continue;

				curPath += (curPath.Length == 1 ? "" : (l ? "/" : "\\")) + comp;
				curBranch = curBranch.ChildBranches.Find(x => (string)x.Model == curPath);

				if (curBranch == null) {
					curPath = curPath.Remove(curPath.Length - comp.Length - 1, comp.Length + 1);
					break;
				}

				curBranch.FetchChildren();
			}
			tlv_dirs.Reveal(curPath, true);
		}

		private async void b_newFolder_Click(object sender, EventArgs e)
		{
			string val;
			InputBox.Launch("RT#", "Enter folder name", false, out val);
			var path = (string)tlv_dirs.SelectedObject;

			try {
				await SDS.CreateRemoteDirectory(Utils.PathCombine(path, val));
			} catch (Exception ex) {
				PluginBase.RTSharpUIExports.Log(LOG_LEVEL.ERROR, "Failed to create remote directory");
				PluginBase.RTSharpUIExports.LogException(LOG_LEVEL.ERROR, ex);
				return;
			}
			
			tlv_dirs.RefreshObject(path);
		}
		
		// HACKHACK
		string CurPath;

		public string ShowDialogWResult()
		{
			DialogResult res = ShowDialog();
			if (res == DialogResult.OK)
				return CurPath;
			return "";
		}

		private void SelectRemoteFolder_Load(object sender, EventArgs e)
		{
			
		}

		private void b_ok_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Close();
		}

		private void b_cancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		private void tlv_dirs_SelectedIndexChanged(object sender, EventArgs e)
		{
			CurPath = (string)tlv_dirs.SelectedObject;
		}
	}
}
