﻿namespace RTSharpIFace {
	partial class WaitingBox {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.pbar_all = new System.Windows.Forms.ProgressBar();
			this.pbar_single = new System.Windows.Forms.ProgressBar();
			this.l_status = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// pbar_all
			// 
			this.pbar_all.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pbar_all.Location = new System.Drawing.Point(0, 54);
			this.pbar_all.Name = "pbar_all";
			this.pbar_all.Size = new System.Drawing.Size(456, 23);
			this.pbar_all.TabIndex = 0;
			// 
			// pbar_single
			// 
			this.pbar_single.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pbar_single.Location = new System.Drawing.Point(0, 44);
			this.pbar_single.Name = "pbar_single";
			this.pbar_single.Size = new System.Drawing.Size(456, 10);
			this.pbar_single.TabIndex = 1;
			// 
			// l_status
			// 
			this.l_status.Dock = System.Windows.Forms.DockStyle.Left;
			this.l_status.Location = new System.Drawing.Point(0, 0);
			this.l_status.Name = "l_status";
			this.l_status.Size = new System.Drawing.Size(456, 44);
			this.l_status.TabIndex = 2;
			this.l_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// WaitingBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(456, 77);
			this.Controls.Add(this.l_status);
			this.Controls.Add(this.pbar_single);
			this.Controls.Add(this.pbar_all);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "WaitingBox";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "RT# - Processing...";
			this.TopMost = true;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WaitingBox_FormClosing);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ProgressBar pbar_all;
		private System.Windows.Forms.ProgressBar pbar_single;
		private System.Windows.Forms.Label l_status;
	}
}