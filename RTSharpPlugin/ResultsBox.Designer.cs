﻿namespace RTSharpIFace {
	partial class ResultsBox {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.t_text = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// t_text
			// 
			this.t_text.Dock = System.Windows.Forms.DockStyle.Fill;
			this.t_text.Location = new System.Drawing.Point(0, 0);
			this.t_text.Multiline = true;
			this.t_text.Name = "t_text";
			this.t_text.ReadOnly = true;
			this.t_text.Size = new System.Drawing.Size(496, 269);
			this.t_text.TabIndex = 0;
			// 
			// ResultsBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(496, 269);
			this.Controls.Add(this.t_text);
			this.Name = "ResultsBox";
			this.ShowIcon = false;
			this.Text = "RT# - Execution results";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox t_text;
	}
}